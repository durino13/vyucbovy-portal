<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDSocialProviderTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_social_provider
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_social_provider', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('social_provider_id');
            $table->integer('user_id')->unsigned();
            $table->string('source', 255)->nullable()->default(null);
            $table->string('source_id', 255)->nullable()->default(null);
            $table->string('avatar', 255)->nullable()->default(null);
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_tbl_d_social_provider_1_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_social_provider');
     }
}
