<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDArticleTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_article
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_article', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255);
            $table->string('alias', 255);
            $table->mediumText('article_text');
            $table->integer('user_id')->unsigned();
            $table->integer('status_id')->unsigned()->default('1');
            $table->dateTime('start_publishing')->nullable()->default(null);
            $table->dateTime('finish_publishing')->nullable()->default(null);
            $table->tinyInteger('archive')->nullable()->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('user_id', 'articles_user_id_foreign')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('status_id', 'fk_articles_1_idx')
                ->references('status_id')->on('tbl_c_status')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_article');
     }
}
