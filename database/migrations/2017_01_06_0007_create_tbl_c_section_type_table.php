<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCSectionTypeTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_c_question_type
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_c_section_type', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('section_type_id');
            $table->enum('type', ['TYPE_QUESTION', 'TYPE_CONTENT']);
            $table->string('name', 255);
            $table->string('description', 255)->nullable()->default(null);
            $table->string('code', 255);
            $table->tinyInteger('gui_order');
            $table->dateTime('valid_from')->default('1900-01-01 00:00:00');
            $table->dateTime('valid_to')->default('9999-01-01 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_c_section_type');
     }
}
