<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCCategoryTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_c_category
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_c_category', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->dateTime('valid_from');
            $table->dateTime('valid_to');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_c_category');
     }
}
