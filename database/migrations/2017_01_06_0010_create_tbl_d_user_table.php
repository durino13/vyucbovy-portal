<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDUserTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_user
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('user_id');
//            $table->integer('status_id')->unsigned()->nullable()->default(null);
            $table->string('email', 255);
            $table->string('password', 255)->nullable()->default(null);
            $table->string('remember_token', 255)->nullable()->default(null);
            $table->string('first_name', 255)->nullable()->default(null);
            $table->string('last_name', 255)->nullable()->default(null);
            $table->dateTime('birth_date')->nullable()->default(null);
            $table->enum('gender', ['male', 'female'])->nullable()->default(null);

            $table->unique(["email"], 'unique_tbl_d_user');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_user');
     }
}
