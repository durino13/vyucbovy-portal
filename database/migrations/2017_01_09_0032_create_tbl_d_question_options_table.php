<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDQuestionOptionsTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_question_options
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_question_options', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('question_option_id');
            $table->integer('question_id')->unsigned();
            $table->integer('order');
            $table->text('option_text');
            $table->string('regex_pattern', 255)->nullable()->default(null);
            $table->tinyInteger('is_correct');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('question_id', 'fk_tbl_d_question_options_1_idx')
                ->references('question_id')->on('tbl_d_question')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_question_options');
     }
}
