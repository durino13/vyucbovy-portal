<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDSectionQuestionGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_section_question_group', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('section_question_group_id');
            $table->integer('morph_id')->unsigned();
            $table->text('group_name');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_d_section_question_group');
    }
}
