<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_settings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('settings_id');
            $table->string('key', 255)->nullable()->default(null);
            $table->string('value', 255)->nullable()->default(null);
            $table->string('type', 255)->nullable()->default(null);
            $table->unique(["key"], 'unique_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_d_settings');
    }
}
