<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDSectionTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_section
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_section', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('section_id');
            $table->integer('lesson_id')->unsigned();
            $table->string('name');
            $table->string('identifier', 255)->unique();
            $table->integer('morph_id');
            $table->string('morph_type', 255);    // Based on this, I will know, in which table I will look for the relation (based on section_id)
            $table->tinyInteger('gui_order');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('lesson_id', 'fk_tbl_d_section_1_idx')
                ->references('lesson_id')->on('tbl_d_lesson')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
         DB::statement('SET FOREIGN_KEY_CHECKS = 0');
         Schema::dropIfExists('tbl_d_section');
         DB::statement('SET FOREIGN_KEY_CHECKS = 1');
     }
}
