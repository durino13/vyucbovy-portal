<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_r_user_role
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_r_user_role', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->integer('role_id')->unsigned()->nullable()->default(null);

            $table->foreign('user_id', 'fk_tbl_r_user_role_1_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('role_id', 'fk_tbl_r_user_role_2_idx')
                ->references('role_id')->on('tbl_c_role')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_r_user_role');
     }
}
