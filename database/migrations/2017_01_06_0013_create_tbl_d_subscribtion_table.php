<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDSubscribtionTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_subscribtion
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_subscribtion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('subscribtion_id');
            $table->integer('user_id')->unsigned();
            $table->integer('course_id')->unsigned()->nullable()->default(null);
            $table->integer('plan_id')->unsigned()->nullable()->default(null);
            $table->dateTime('subscribed_on');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('user_id', 'fk_tbl_d_subscribtion_1_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('course_id', 'fk_tbl_d_subscribtion_2_idx')
                ->references('course_id')->on('tbl_d_course')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('plan_id', 'fk_tbl_d_subscribtion_3_idx')
                ->references('plan_id')->on('tbl_d_plan')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_subscribtion');
     }
}
