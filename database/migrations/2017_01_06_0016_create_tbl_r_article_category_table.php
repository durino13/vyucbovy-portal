<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRArticleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_r_article_category
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_r_article_category', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('article_id')->unsigned();
            $table->integer('category_id')->unsigned();


            $table->foreign('article_id', 'articles_categories_articles')
                ->references('id')->on('tbl_d_article')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('category_id', 'articles_categories_categories')
                ->references('id')->on('tbl_c_category')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_r_article_category');
     }
}
