<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDCourseTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_course
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_course', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('course_id');
            $table->integer('subject_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->integer('difficulty_id')->unsigned()->nullable()->default(null);
//            $table->integer('status_id')->unsigned();
            $table->string('identifier', 100)->unique();
            $table->string('name', 255);
            $table->string('description', 2000);
            $table->decimal('price', 18, 2)->nullable()->default(null);
            $table->string('duration', 255)->nullable()->default(null);
            $table->string('intro_image_url', 255)->nullable()->default(null);
            $table->string('intro_video_url', 255)->nullable()->default(null);
            $table->tinyInteger('is_published')->default(0);
            $table->tinyInteger('is_archived')->default(0);
            $table->tinyInteger('is_free_of_charge')->default(0);
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('difficulty_id', 'fk_tbl_d_course_1_idx')
                ->references('difficulty_id')->on('tbl_c_difficulty')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('author_id', 'fk_tbl_d_course_3_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('subject_id', 'fk_tbl_d_topic_1_idx')
                ->references('subject_id')->on('tbl_c_subject')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_course');
     }
}
