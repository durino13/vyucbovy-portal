<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDLessonTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_lesson
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_lesson', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('lesson_id');
            $table->integer('course_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->integer('module_id')->unsigned()->nullable()->default(null);
            $table->string('name', 255)->nullable()->default(null);
            $table->string('description', 1000)->nullable()->default(null);
            $table->string('identifier', 255)->unique();
            $table->integer('gui_order')->nullable()->default(null);
            $table->string('intro_image_url', 255)->nullable()->default(null);
            $table->dateTime('start_publishing')->nullable()->default(null);
            $table->dateTime('finish_publishing')->nullable()->default(null);
            $table->tinyInteger('is_published')->default(0);
            $table->tinyInteger('is_archived')->default(0);
            $table->tinyInteger('is_free_of_charge')->default(0);
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('author_id', 'fk_tbl_d_lesson_1_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('course_id', 'fk_tbl_d_lesson_2_idx')
                ->references('course_id')->on('tbl_d_course')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('module_id', 'fk_tbl_d_lesson_3_idx')
                ->references('module_id')->on('tbl_d_module')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_lesson');
     }
}
