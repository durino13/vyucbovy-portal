<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRTutorageTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_r_tutorage
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_r_tutorage', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('course_id')->unsigned();


            $table->foreign('user_id', 'fk_tbl_r_tutorage_1_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('course_id', 'fk_tbl_r_tutorage_2_idx')
                ->references('course_id')->on('tbl_d_course')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_r_tutorage');
     }
}
