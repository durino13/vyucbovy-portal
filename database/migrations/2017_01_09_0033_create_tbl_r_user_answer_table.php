<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRUserAnswerTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_r_user_answer
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_r_user_answer', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('user_answer_id');
            $table->integer('question_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('answer')->nullable()->default(null);
            $table->integer('order');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id', 'fk_tbl_r_user_answer_1_idx')
                ->references('user_id')->on('tbl_d_user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('question_id', 'fk_tbl_user_answer_1_idx')
                ->references('question_id')->on('tbl_d_question')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_r_user_answer');
     }
}
