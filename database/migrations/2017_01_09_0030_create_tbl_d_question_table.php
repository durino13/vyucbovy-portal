<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDQuestionTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_d_question
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_question', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('question_id');
            $table->integer('section_question_group_id')->unsigned();
            $table->integer('section_type_id')->unsigned();
            $table->text('question');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('section_question_group_id', 'fk_tbl_d_section_question_1_idx')
                ->references('section_question_group_id')->on('tbl_d_section_question_group')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('section_type_id', 'fk_tbl_d_question_2_idx')
                ->references('section_type_id')->on('tbl_c_section_type')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_question');
     }
}
