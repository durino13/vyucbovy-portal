<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDVimeoCacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_vimeo_cache', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('vimeo_cache_id');
            $table->string('url');
            $table->string('params')->nullable()->default(null);
            $table->text('response');
            $table->dateTime('expires');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_d_vimeo_cache');
    }
}
