<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCSubjectTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_c_subject
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_c_subject', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('subject_id');
            $table->string('name', 255);
            $table->string('description', 255)->nullable()->default(null);
            $table->string('code', 255);
            $table->string('identifier', 255)->unique();
            $table->tinyInteger('gui_order');
            $table->dateTime('valid_from')->default('1900-01-01 00:00:00');
            $table->dateTime('valid_to')->default('9999-01-01 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_c_subject');
     }
}
