<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDModuleTable extends Migration
{
    /**
     * Run the migrations.
     * @table tbl_c_topic_group
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_module', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('module_id');
            $table->string('name', 255);
            $table->string('description', 2000)->nullable()->default(null);
            $table->string('identifier', 255)->unique();
            $table->tinyInteger('gui_order');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->dateTime('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('tbl_d_module');
     }
}
