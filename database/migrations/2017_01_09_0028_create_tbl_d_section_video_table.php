<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDSectionVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_d_section_video', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('section_video_id');
            $table->integer('morph_id')->unique();
            $table->string('url', 255);
            $table->time('length');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_d_section_video');
    }
}
