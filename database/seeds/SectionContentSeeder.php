<?php

use Illuminate\Database\Seeder;

class SectionContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create audio sections

        DB::table('tbl_d_section_content')->insert([

            [
                'section_content_id' => 1,
                'morph_id' => 1,
                'content' => 'Some content ...'
            ]

        ]);
    }
}
