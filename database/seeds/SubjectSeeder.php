<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_c_subject')->insert([

            [
                'name' => 'Matematika',
                'description' => 'Kurzy a doučovania z matematiky',
                'code' => 'MATH',
                'identifier' => 'matematika',
                'gui_order' => '1',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],
            [
                'name' => 'Angličtina',
                'description' => 'Kurzy a doučovania z angličtiny',
                'code' => 'ENGLISH',
                'identifier' => 'anglictina',
                'gui_order' => '5',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],
            [
                'name' => 'Francúzština',
                'description' => 'Kurzy a doučovania z francúzštiny',
                'code' => 'FRENCH',
                'identifier' => 'francuzstina',
                'gui_order' => '10',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],
            [
                'name' => 'Nemčina',
                'description' => 'Kurzy a doučovania z nemčiny',
                'code' => 'GERMAN',
                'identifier' => 'nemcina',
                'gui_order' => '15',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ]

        ]);
    }
}
