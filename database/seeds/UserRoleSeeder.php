<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vloz role pre uzivatela
        DB::table('tbl_r_user_role')->insert([
            [
                'user_id' => '1',
                'role_id' => '3'
            ],
            [
                'user_id' => '2',
                'role_id' => '2'
            ],
            [
                'user_id' => '1',
                'role_id' => '2'
            ]
        ]);
    }
}
