<?php

use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create lesson
        DB::table('tbl_d_lesson')->insert([
            [
                'course_id' => '1',
                'author_id' => '1',
//                'status_id' => '1',
                'module_id' => '1',
                'name' => 'Origins of algebra',
                'description' => 'Origins of algebra description',
                'identifier' => 'origins-of-algebra',
                'gui_order' => '1',
                'intro_image_url' => 'https://cdn.kastatic.org/images/khan-logo-vertical-transparent.png',
                'start_publishing' => '2017-01-01 00:00:00',
                'finish_publishing' => '9999-01-01 00:00:00',
            ],
            [
                'course_id' => '1',
                'author_id' => '1',
//                'status_id' => '1',
                'module_id' => '2',
                'name' => 'Abstractness',
                'description' => 'Abstractness description',
                'identifier' => 'abstractness',
                'gui_order' => '2',
                'intro_image_url' => 'https://cdn.kastatic.org/images/khan-logo-vertical-transparent.png',
                'start_publishing' => '2017-01-01 00:00:00',
                'finish_publishing' => '9999-01-01 00:00:00',
            ],
            [
                'course_id' => '1',
                'author_id' => '1',
//                'status_id' => '1',
                'module_id' => '2',
                'name' => 'The Beauty of Algebra',
                'description' => 'The Beauty of Algebra description',
                'identifier' => 'algebra-beauty',
                'gui_order' => '3',
                'intro_image_url' => 'https://cdn.kastatic.org/images/khan-logo-vertical-transparent.png',
                'start_publishing' => '2017-01-01 00:00:00',
                'finish_publishing' => '9999-01-01 00:00:00',
            ]

        ]);

    }
}
