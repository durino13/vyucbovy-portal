<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create section in the 1st lesson ...

        DB::table('tbl_d_section')->insert([

            [
                'lesson_id' => '1',
                'name' => 'Video',
                'morph_id' => 1,
                'morph_type' => 'App\SectionVideo',
                'identifier' => 'early-math-video-1',
                'gui_order' => '1',
            ],
            [
                'lesson_id' => '1',
                'name' => 'Some content',
                'morph_id' => 1,
                'morph_type' => 'App\SectionContent',
                'identifier' => 'early-math-content-1',
                'gui_order' => '2',
            ],
            [
                'lesson_id' => '1',
                'name' => 'Audio',
                'morph_id' => 1,
                'morph_type' => 'App\SectionAudio',
                'identifier' => 'early-math-audio-1',
                'gui_order' => '3',
            ],
            [
                'lesson_id' => '1',
                'name' => 'Question (random)',
                'morph_id' => 1,
                'morph_type' => 'App\SectionQuestionGroup',
                'identifier' => 'early-math-question-group-1',
                'gui_order' => '4',
            ]

        ]);

    }
}

