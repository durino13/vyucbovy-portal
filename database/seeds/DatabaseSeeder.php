<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Enumerators
        $this->call(DifficultySeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(SectionTypeSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(SubjectSeeder::class);

        // Mock data
        $this->call(UserSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(LessonSeeder::class);
        $this->call(SectionSeeder::class);
        $this->call(SectionVideoSeeder::class);
        $this->call(SectionAudioSeeder::class);
        $this->call(SectionContentSeeder::class);
        $this->call(SectionQuestionGroupSeeder::class);
        $this->call(SettingsSeeder::class);
    }
}
