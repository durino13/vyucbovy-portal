<?php

use Illuminate\Database\Seeder;

class SectionQuestionGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_d_section_question_group')->insert([

            [
                'group_name' => 'Arithmetic questions',
                'morph_id' => 1,
            ]

        ]);
    }
}
