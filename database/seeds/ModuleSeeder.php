<?php

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create lesson_topic
        DB::table('tbl_d_module')->insert([
            [
                'name' => 'Introduction to Algebra',
                'description' => 'Introduction to Algebra description',
                'identifier' => 'intro-to-algebra',
                'gui_order' => '1'
            ],
            [
                'name' => 'Solving basic equations',
                'description' => 'Solving basic equations description',
                'identifier' => 'solving-basic-equations',
                'gui_order' => '2'
            ],
            [
                'name' => 'Linear equations',
                'description' => 'Linear equations description',
                'identifier' => 'linear-equations',
                'gui_order' => '3'
            ]
        ]);
    }
}
