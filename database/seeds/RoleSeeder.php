<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_c_role')->insert([

            [
                'name' => 'Študent',
                'description' => 'Študent EWB-Onlineacademy',
                'code' => 'STUDENT',
                'gui_order' => '1',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'name' => 'Lektor',
                'description' => 'Lektor v EWB-Onlineacademy',
                'code' => 'LECTOR',
                'gui_order' => '5',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'name' => 'Administrátor',
                'description' => 'Administrátor webu a aplikácie',
                'code' => 'ADMIN',
                'gui_order' => '10',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ]

        ]);
    }
}
