<?php

use Illuminate\Database\Seeder;

class SectionVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create video sections

        DB::table('tbl_d_section_video')->insert([

            [
                'section_video_id' => 1,
                'morph_id' => 1,
                'url' => 'http://www.sme.sk',
                'length' => 12
            ]

        ]);
    }
}
