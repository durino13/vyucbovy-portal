<?php

use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create course
        DB::table('tbl_d_course')->insert([
            'subject_id' => '1',
            'author_id' => '1',
            'difficulty_id' => '1',
//            'status_id' => '1',
            'identifier' => 'early-math',
            'name' => 'Early Math',
            'description' => 'Early Math description',
            'price' => '35',
            'duration' => '1 week',
            'intro_image_url' => 'https://vuejs.org/images/logo.png',
            'intro_video_url' => 'https://player.vimeo.com/video/110807219?color=c9ff23&byline=0'
        ]);
    }
}
