<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_c_status')->insert([
            [
                'name' => 'Publikovaný',
                'description' => '',
                'code' => 'PUBLISHED',
                'gui_order' => '1',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'name' => 'Nepublikovaný',
                'description' => '',
                'code' => 'UNPUBLISHED',
                'gui_order' => '5',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],

            [
                'name' => 'Archivovaný',
                'description' => '',
                'code' => 'ARCHIVED',
                'gui_order' => '10',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],

            [
                'name' => 'Zmazaný',
                'description' => '',
                'code' => 'DELETED',
                'gui_order' => '15',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ]
        ]);

    }
}
