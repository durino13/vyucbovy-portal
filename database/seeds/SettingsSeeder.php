<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_d_settings')->insert([

            [
                'key' => 'vimeo_access_token',
                'value' => 'fbd3ddd28253f7de0160cc14a842b840',
                'type' => 'STRING'
            ],
            [
                'key' => 'vimeo_user_id',
                'value' => '67982293',
                'type' => 'INT'
            ]

        ]);
    }
}
