<?php

use Illuminate\Database\Seeder;

class SectionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_c_section_type')->insert([

            [
                'type' => 'TYPE_CONTENT',
                'name' => 'Audio',
                'description' => '',
                'code' => 'AUDIO',
                'gui_order' => '5',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],

            [
                'type' => 'TYPE_CONTENT',
                'name' => 'Video',
                'description' => '',
                'code' => 'VIDEO',
                'gui_order' => '10',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],

            [
                'type' => 'TYPE_CONTENT',
                'name' => 'Content',
                'description' => '',
                'code' => 'CONTENT',
                'gui_order' => '15',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01'
            ],

            [
                'type' => 'TYPE_QUESTION',
                'name' => 'Pravda / nepravda',
                'description' => 'Odpoveď na otázku sa vyberá z dvoch možností - pravda / nepravda',
                'code' => 'TRUE_FALSE',
                'gui_order' => '1',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'type' => 'TYPE_QUESTION',
                'name' => 'Jedna správna odpoveď',
                'description' => 'Existuje iba jedna správna odpoveď na otázku',
                'code' => 'ONE_OPTION',
                'gui_order' => '5',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'type' => 'TYPE_QUESTION',
                'name' => 'Viac správnych odpovedí',
                'description' => 'Existuje viac správnych odpovedí na otázku. Pre správne zodpovedanie otázky je nutné vybrať všetky možné správne odpovede',
                'code' => 'MULTIPLE_OPTIONS',
                'gui_order' => '10',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'type' => 'TYPE_QUESTION',
                'name' => 'Doplňovanie do prázdnych polí',
                'description' => 'Odpoveď na otázku formou doplnenia hodnoty do prázdneho poľa',
                'code' => 'FILL_IN_THE_BLANKS',
                'gui_order' => '15',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'type' => 'TYPE_QUESTION',
                'name' => 'Esej',
                'description' => 'Odpoveď na otázku formou eseje',
                'code' => 'ESSAY',
                'gui_order' => '20',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ]

        ]);
    }
}
