<?php

use Illuminate\Database\Seeder;

class DifficultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_c_difficulty')->insert([

            [
                'name' => 'Začiatočník',
                'description' => '',
                'code' => 'BEGINNER',
                'gui_order' => '1',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'name' => 'Pokročilý',
                'description' => '',
                'code' => 'INTERMEDIATE',
                'gui_order' => '5',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ],

            [
                'name' => 'Expert',
                'description' => '',
                'code' => 'EXPERT',
                'gui_order' => '10',
                'valid_from' => '1900-01-01',
                'valid_to' => '9999-01-01',
            ]

        ]);
    }
}
