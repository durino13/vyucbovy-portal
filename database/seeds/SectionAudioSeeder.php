<?php

use Illuminate\Database\Seeder;

class SectionAudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create audio sections

        DB::table('tbl_d_section_audio')->insert([

            [
                'section_audio_id' => 1,
                'morph_id' => 1,
                'url' => 'http://www.sme.sk',
                'length' => 12
            ]

        ]);
    }
}
