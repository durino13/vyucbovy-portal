<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

    public function run()
    {
        DB::table('tbl_d_user')->insert([
            [
//                'status_id' => '10',
                'email' => 'durino13@gmail.com',
                'password' => '$2y$10$xVzoUIXSg5VjA3RuN91.we7RmbdOCeKHNkPTiHYQo0/hblLjTfYwu',
                'remember_token' => null,
                'first_name' => 'Juraj',
                'last_name' => 'Marušiak',
                'birth_date' => '1980-10-18',
                'gender' => 'male',
            ],
            [
//                'status_id' => '10',
                'email' => 'lucy.marusiakova@gmail.com',
                'password' => '',
                'remember_token' => null,
                'first_name' => 'Lucia',
                'last_name' => 'Marušiaková',
                'birth_date' => '1981-06-03',
                'gender' => 'female',
            ],
        ]);
    }

}