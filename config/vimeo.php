<?php

/*
 * This file is part of Laravel Vimeo.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'development',

    /*
    |--------------------------------------------------------------------------
    | Vimeo Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'production' => [
            'client_id' => 'your-client-id',
            'client_secret' => 'your-client-secret',
            'access_token' => null,
        ],

        'development' => [
            'client_id' => 'a795076e22f2922e777a9b92b01e529f1fd1581e',
            'client_secret' => 'jaVE5haRpJV0IjBxvYlRQZA5pScf/tyTQ25shpWXdFDMfDKPHU0t27TkJSi1nJKotNiHM69YNMaHFT1XH8azdBHYnCDA4KezuuFaxtN+toaNMd18KAXCn4x2PGxZTQhg',
            'access_token' => '6a4c9195487d1c99a74e378aa18f6001',
        ],

    ],

];
