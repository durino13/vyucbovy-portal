<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//-----------------------------------------------------------------
// Authentication
//-----------------------------------------------------------------

// Auth routes
Route::auth();

Route::get('zabudnute/heslo', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('forgot.password.index');
Route::post('zabudnute/heslo', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('forgot.password.email');
Route::get('heslo/reset', 'Auth\PasswordController@showResetForm')->name('forgot.password.reset');
Route::post('heslo/reset', 'Auth\ResetPasswordController@reset')->name('forgot.password.reset.password');

// Register route
Route::get('register', 'Site\RegisterController@index')->name('register.index');

// Socialite auth
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider')->name('provider.login.or.sync');
Route::get('auth/{provider}/callback','Auth\AuthController@handleProviderCallback');

//-----------------------------------------------------------------
// Courses
//-----------------------------------------------------------------

// Courses routes
Route::get('predmet/matematika','Site\HomeController@math')->name('math');
Route::get('predmet/anglictina','Site\HomeController@english')->name('english');

//-----------------------------------------------------------------
// Settings
//-----------------------------------------------------------------

Route::get('uzivatel/nastavenia', 'Site\UserController@index')->name('user.index');

//-----------------------------------------------------------------
// Site routes
//-----------------------------------------------------------------

Route::get('/', 'Site\HomeController@index')->name('index');
Route::get('/nova', 'Site\HomeController@nova')->name('nova');

//-----------------------------------------------------------------
// Lektor controller
//-----------------------------------------------------------------

Route::get('/portal', 'Lector\LectorController@index')->name('lector.index');
Route::get('/portal/matematika', 'Site\LectorController@math')->name('site.lector.math');
Route::get('/portal/anglictina', 'Site\HomeController@english')->name('site.lector.english');
Route::get('/portal/francuzstina', 'Site\HomeController@french')->name('site.lector.french');

//-----------------------------------------------------------------
// Resource routes ..
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Api routes
//-----------------------------------------------------------------

// TODO Kvoli tomu, aby som mohol robit request cez POSTMANA, tak som docasne vypol authentifikaciu ..
Route::group(['prefix' => 'api', 'as' => 'api.',/* 'middleware' => ['auth']*/], function () {
    Route::resource('subject', 'Api\SubjectController');
    Route::resource('course', 'Api\CourseController');
    Route::resource('lesson', 'Api\LessonController');
    Route::resource('module', 'Api\ModuleController');
    Route::resource('section', 'Api\SectionController');

    // Vimeo
    Route::get('/vimeo/albums', 'Api\VimeoController@getAlbums');
    Route::get('/vimeo/albums/{albumID}/videos', 'Api\VimeoController@getVideosInAlbum');
    Route::get('/vimeo/albums/name/{name}', 'Api\VimeoController@getAlbumByName');

    // Route::get('{lesson}', 'Api\LessonController@getByCode');   //TODO Vyzera byt ako catchall, prerobit, zachytava metody, ktore nechcem ..
});

//----------------------------------------------------------
// Admin routes
//----------------------------------------------------------

// Admin routes
Route::group(['prefix' => 'administrator', 'as' => 'administrator.', 'middleware' => ['auth', 'admin']], function () {

    // Home route
    Route::get('/','Admin\ArticleController@index')->name('index');

    // Article routes
    Route::get('article/archive', 'Admin\ArticleController@listarchive')->name('article.archive.index');
    Route::post('article/{article}/archive/archive', 'Admin\ArticleController@archive')->name('article.archive.archive');
    Route::post('article/{article}/archive/restore', 'Admin\ArticleController@restore')->name('article.archive.restore');
    Route::resource('article', 'Admin\ArticleController');

    // Trash routes
    Route::get('trash', 'Admin\TrashController@index')->name('trash.index');
    Route::post('trash/{item}/restore', 'Admin\TrashController@restore')->name('trash.restore');
    Route::delete('trash/{item}/destroy', 'Admin\TrashController@destroy')->name('trash.destroy');

    // Category routes
    Route::resource('category', 'Admin\CategoryController');

    // Settings routes
    Route::resource('settings', 'Admin\SettingsController');

    // User routes
    Route::resource('user', 'Admin\UserController');

});
