@extends('admin.layouts.main')
@section('content')

    {{--Open form--}}

    <?php if (isset($settings)) { ?>
    {{ Form::model($settings, ['id' => 'settings-form'/*, 'route' => ['administrator.settings.update', $settings->settings_id]*/]) }}
    <?php } else { ?>
    {{ Form::open(['id' => 'settings-form', 'url' => '/administrator/settings']) }}
    <?php } ?>

    {{--Hidden fields--}}

    <input type="hidden" name="settings_id" value="{{ isset($settings->settings_id) ? $settings->settings_id : '' }}">

    {{--Toolbar--}}

    <div class="toolbar">
        <button id="settings_save_and_close" name="action" value="save_and_close" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Save & Close</button>
        <button id="settings_save" name="action" value="save" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Save</button>
        <a id="form-close" href="#" class="btn btn-danger btn-sm" data-redirect="{{ route('administrator.settings.index') }}"><i class="fa fa-close"></i> Close</a>
    </div>

    {{--Status & error messages--}}

    @include('admin.common.message')

    {{--Content--}}

    <div class="content">

        <div class="box box-default">

            <div class="box-header with-border">
                <h3 class="box-title">Settings form</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                    </button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Key:</label>
                            <?php echo Form::text('key', null, ['class' => 'form-control']); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Value:</label>
                            <?php echo Form::text('value', null, ['class' => 'form-control']); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Type:</label>
                            <?php echo Form::text('type', null, ['class' => 'form-control']); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{ Form::close() }}

@endsection