<?php use Illuminate\Support\Facades\Route; ?>
@extends('admin.layouts.main')
@section('content')

{{-- Toolbar --}}

<div class="toolbar">
    <a id="new_settings" href="/administrator/settings/create" class="btn btn-success btn-sm"><span class="fa fa-plus-circle"></span> New key</a>
</div>

{{--Status & error messages--}}

@include('admin.common.message')

{{--Content--}}

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Settings</h3>
                </div>

                <div class="box-body">

                    <?php

                    (Route::getFacadeRoot()->current()->uri() === 'administrator/article/archive') ? $idName = "dt-archive" : $idName = "dt-articles";

                    ?>

                    <table id={{ $idName }} class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            {{--<th class="select-checkbox"></th>--}}
                            <th></th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Type</th>
                            <th>Updated</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Type</th>
                            <th>Updated</th>
                            <th>Created</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            @foreach($settings as $setting)
                                <tr id="{{ $setting->id }}">
                                    <td></td>
                                    <td>
                                        <a href="/administrator/settings/{{ $setting->settings_id }}/edit">{{ $setting->key }}</a><br/>
                                    </td>
                                    <td>
                                        <div class="flex-container">
                                            <div>
                                                {{ $setting->value }}
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $setting->type }}</td>
                                    <td>{{ $setting->updated_at }}</td>
                                    <td>{{ $setting->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Control Sidebar -->
{{--@include('admin.common.right-menu')--}}