@extends('admin.layouts.main')
@section('content')

{{-- Toolbar --}}

<div class="toolbar">
    <a href="{{ route('administrator.user.create') }}" class="btn btn-success btn-sm"><span class="fa fa-plus-circle"></span> New user</a>
</div>

{{--Status & error messages--}}

@include('admin.common.message')

{{--Content--}}

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">User list</h3>
                </div>

                <div class="box-body">
                    <table id="dt-users" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Updated</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr id="{{ $user->user_id }}">
                                    <td>{{ $user->user_id }}</td>
                                    <td><a href="{{ route('administrator.user.edit', $user->user_id) }}">{{ $user->name }}</a></td>
                                    <td>{{ $user->updated_at }}</td>
                                    <td>{{ $user->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection