@extends('site.layouts_v1.main')

@section('content')

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-7" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">Brand</a></div>
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-7">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Predmety</a></li>
                        <li><a href="#">Cenník</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Kontakt</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <section class="intro">
        <div class="content">

            <div class="row">

                {{-- Site title --}}

                <div class="col-md-6 col-md-offset-1">

                    <h1 data-shadow-text="Douč ma">Doučím.sa</h1>
                    <p style="width: 500px;">Doučovanie matematiky a jazykov online. Lorem ipsum dolor sit amet,
                        harum illo provident recusandae sint. A amet doloribus eveniet facilis inventore</p>
                    <div id="intro_image_url"></div>

                </div>

                {{-- Login form --}}

                <div class="col-md-5">

                    @if($errors)
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endif

                    <form id="login-form" action="login" method="post">
                        {{ csrf_field() }}
                        <input checked id='signin' name='action' type='radio' value='signin'>
                        <label for='signin' style="color: #FE4B74">Prihlásiť</label>
                        <input id='signup' name='action' type='radio' value='signup'>
                        <label for='signup' style="color: #FE4B74">Registruj sa</label>
                        <input id='reset' name='action' type='radio' value='reset'>
                        <label for='reset' style="color: #FE4B74">Reset</label>
                        <div id='wrapper'>
                            <div id='arrow'></div>
                            <input id='email' placeholder='Email' type='text' name="email">
                            <input id='pass' placeholder='Heslo' type='password' name="password">
                            <input id='repass' placeholder='Zopakuj heslo' type='password'>
                        </div>
                        <button type='submit'>
                            <span>
                              Zabudol som heslo
                              <br>
                              Prihlásiť sa
                              <br>
                              Prihlásiť sa
                            </span>
                        </button>
                        <div class="flex-container or">-- or --</div>
                    </form>
                    <form id="facebook-form" action="auth/facebook" method="get">
                        <button class="loginBtn--facebook">Login with Facebook</button>
                    </form>

                </div>

            </div>

            {{-- Owl--}}

            <div class='owl'>
                <div class='body'>
                    <div class='wing'></div>
                    <div class='wing'></div>
                    <div class='feet'></div>
                    <div class='feet right'></div>
                    <div class='feather'></div>
                </div>
                <div class='head'>
                    <div class='eyes'>
                        <div class='beak'></div>
                        <div class='eye'>
                            <div class='pupil'></div>
                        </div>
                        <div class='eye'>
                            <div class='pupil'></div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </section>

    <section>
        <div class="content">
            <h1>Resize your browser and see how they adapt.</h1>
        </div>
    </section>

    <footer>
        Made by <a href="http://www.twitter.com/ckor">@ckor</a>
    </footer>


@endsection