@extends('site.layouts.one-column-no-header')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <form class="form box-shadow">

                <div class="form-header">
                    <h2>Nový užívateľ</h2>
                </div>

                <div class="form-content">
                    <ul>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Meno</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input type="text">
                                        <span>type your tool, separated by comma or space</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Priezvisko</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input type="text">
                                        <span>type your tool, separated by comma or space</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Email</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input type="text">
                                        <span>type your tool, separated by comma or space</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Heslo</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input type="text">
                                        <span>type your tool, separated by comma or space</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Zopakuj heslo</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input type="text">
                                        <span>type your tool, separated by comma or space</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
                                Vytvoriť užívateľa
                            </button>
                        </div>

                    </ul>
                </div>

            </form>

        </div>
    </div>

@endsection