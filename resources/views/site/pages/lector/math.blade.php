@extends('site.layouts.one-column-no-header')

@section('menu')

@endsection

@section('header')

    <section class="flex-container math">
        <h1 class="math__header">Lector area</h1>
    </section>

@endsection

@section('content')

    <section id="lector-app">

        <div class="row">
            <div class="col-md-12">
                <router-view name="breadcrumbs"></router-view>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-3">
                            <router-view name="leftMenu"></router-view>
                        </div>
                        <div class="col-md-9">
                            <router-view name="rightContent"></router-view>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>

@endsection