<?php
use Illuminate\Support\Facades\Auth;
?>

<header id="header" class="header">
    <div class="container">

        <!-- LOGO -->
        <div class="logo"><a href="{{ route('index') }}"><img src="{{ asset('megacourse/images/logo.png') }}" alt=""></a></div>
        <!-- END / LOGO -->

        <!-- NAVIGATION -->
        <nav class="navigation">

            <div class="open-menu">
                <span class="item item-1"></span>
                <span class="item item-2"></span>
                <span class="item item-3"></span>
            </div>

            <!-- MENU -->
            <ul class="menu">
                <li class="menu-item-has-children">
                    <a href="blog-list.html">Predmety</a>
                    <ul class="sub-menu">
                        <li><a href="{{ route('site.lector.math') }}">Matematika</a></li>
                        <li><a href="{{ route('site.lector.english') }}">Angličtina</a></li>
                        <li><a href="blog-single.html">Franczúština</a></li>
                    </ul>
                </li>
                <li><a href="categories.html">Cenník</a></li>
                <li><a href="categories.html">FAQ</a></li>
                <li><a href="categories.html">Kontakt</a></li>

                @can('view-lector')
                    <li class="menu-item-has-children">
                        <a href="blog-list.html">Lektor</a>
                        <ul class="sub-menu">
                            <li><a href="{{ route('site.lector.math') }}">Matematika</a></li>
                            <li><a href="{{ route('site.lector.english') }}">Angličtina</a></li>
                            <li><a href="{{ route('site.lector.french') }}">Franczúština</a></li>
                        </ul>
                    </li>
                @endcan
                @can('view-dashboard')
                    <li><a href="{{ route('administrator.index') }}">Administrator</a></li>
                @endcan
            </ul>
            <!-- END / MENU -->

            <!-- SEARCH BOX -->
            <div class="search-box">
                <i class="icon md-search"></i>
                <div class="search-inner">
                    <form>
                        <input type="text" placeholder="key words">
                    </form>
                </div>
            </div>
            <!-- END / SEARCH BOX -->

            @if (Auth::check())

                <ul class="list-account-info">

                    <!-- MESSAGE INFO -->
                    <li class="list-item messages">
                        <div class="message-info item-click">
                            <i class="icon md-email"></i>
                            <span class="itemnew"></span>
                        </div>
                        <div class="toggle-message toggle-list">
                            <div class="list-profile-title">
                                <h4>Inbox message</h4>
                                <span class="count-value">3</span>
                                <a href="#" class="new-message"><i class="icon md-pencil"></i></a>
                            </div>
                            <div class="list-wrap ps-container ps-active-y"><ul class="list-message">

                                    <!-- LIST ITEM -->
                                    <li class="ac-new">
                                        <a href="#">
                                            <div class="image">
                                                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
                                            </div>
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>Welcome message</p>
                                                <div class="time">
                                                    <span>12:53</span>
                                                </div>
                                                <div class="indicator">
                                                    <i class="icon md-paperclip"></i>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li class="ac-new">
                                        <a href="#">
                                            <div class="image">
                                                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
                                            </div>
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Name of sender</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>Message title</p>
                                                <div class="time">
                                                    <span>5 days ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li class="ac-new">
                                        <a href="#">
                                            <div class="image">
                                                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
                                            </div>
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Sasha Grey</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>Maecenas sodales, nisl eget dign...</p>
                                                <div class="time">
                                                    <span>5 days ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="image">
                                                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
                                            </div>
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Amanda Gleam</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>Message title</p>
                                                <div class="time">
                                                    <span>5 days ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="image">
                                                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
                                            </div>
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Amanda Gleam</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>Message title</p>
                                                <div class="time">
                                                    <span>5 days ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="image">
                                                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
                                            </div>
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Amanda Gleam</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>Message title</p>
                                                <div class="time">
                                                    <span>5 days ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                </ul><div class="ps-scrollbar-x-rail" style="width: 400px; display: none; left: 0px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 300px; display: inherit; right: 0px;"><div class="ps-scrollbar-y" style="top: 0px; height: 150px;"></div></div></div>
                            <div class="viewall">
                                <a href="#">view all 80 messages</a>
                            </div>
                        </div>
                    </li>
                    <!-- END / MESSAGE INFO -->

                    <!-- NOTIFICATION -->
                    <li class="list-item notification">
                        <div class="notification-info item-click">
                            <i class="icon md-bell"></i>
                            <span class="itemnew"></span>
                        </div>
                        <div class="toggle-notification toggle-list">
                            <div class="list-profile-title">
                                <h4>Notification</h4>
                                <span class="count-value">2</span>
                            </div>

                            <div class="list-wrap ps-container ps-active-y"><ul class="list-notification">

                                    <!-- LIST ITEM -->
                                    <li class="ac-new">
                                        <a href="#">
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>attend Salary for newbie course</p>
                                                <div class="image">
                                                    <img src="{{ asset('megacourse/images/feature/img-1.jpg') }}" alt="">
                                                </div>
                                                <div class="time">
                                                    <span>5 minutes ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li class="ac-new">
                                        <a href="#">
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>attend Salary for newbie course</p>
                                                <div class="image">
                                                    <img src="{{ asset('megacourse/images/feature/img-1.jpg') }}" alt="">
                                                </div>
                                                <div class="time">
                                                    <span>5 minutes ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>attend Salary for newbie course</p>
                                                <div class="image">
                                                    <img src="{{ asset('megacourse/images/feature/img-1.jpg') }}" alt="">
                                                </div>
                                                <div class="time">
                                                    <span>5 minutes ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>attend Salary for newbie course</p>
                                                <div class="image">
                                                    <img src="{{ asset('megacourse/images/feature/img-1.jpg') }}" alt="">
                                                </div>
                                                <div class="time">
                                                    <span>5 minutes ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>attend Salary for newbie course</p>
                                                <div class="image">
                                                    <img src="{{ asset('megacourse/images/feature/img-1.jpg') }}" alt="">
                                                </div>
                                                <div class="time">
                                                    <span>5 minutes ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->

                                    <!-- LIST ITEM -->
                                    <li>
                                        <a href="#">
                                            <div class="list-body">
                                                <div class="author">
                                                    <span>Megacourse</span>
                                                    <div class="div-x"></div>
                                                </div>
                                                <p>attend Salary for newbie course</p>
                                                <div class="image">
                                                    <img src="{{ asset('megacourse/images/feature/img-1.jpg') }}" alt="">
                                                </div>
                                                <div class="time">
                                                    <span>5 minutes ago</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- END / LIST ITEM -->



                                </ul><div class="ps-scrollbar-x-rail" style="width: 400px; display: none; left: 0px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 225px; display: inherit; right: 0px;"><div class="ps-scrollbar-y" style="top: 0px; height: 101px;"></div></div></div>
                        </div>
                    </li>
                    <!-- END / NOTIFICATION -->

                    <li class="list-item account">
                        <div class="account-info item-click">
                            @if (Auth::user()->socialProviders->first())
                                <img src="{{ Auth::user()->socialProviders->first()->avatar }}" alt="">
                            @endif
                        </div>
                        <div class="toggle-account toggle-list">
                            <ul class="list-account">
                                <li><a href="#"><i class="icon md-config"></i>Nastavenia</a></li>
                                <li><a href="{{ route('provider.login.or.sync', 'facebook') }}"><i class="icon md-config"></i>Facebook sync.</a></li>
                                <li id="logout"><a href="#"><i class="icon md-arrow-right"></i>Odhlásiť</a></li>
                            </ul>
                        </div>
                    </li>

                </ul>

        @endif
            <!-- END / LIST ACCOUNT INFO -->

        </nav>
        <!-- END / NAVIGATION -->

    </div>
</header>