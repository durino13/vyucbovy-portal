<section id="before-footer" class="before-footer">
    <div class="container">
        <div class="row">

            <div class="col-lg-8">
                <div class="mc-count-item">
                    <h4>Courses</h4>
                    <p><span class="countup">2536,556</span></p>
                </div>
                <div class="mc-count-item">
                    <h4>Teachers</h4>
                    <p><span class="countup">10,389</span></p>
                </div>
                <div class="mc-count-item">
                    <h4>Students</h4>
                    <p><span class="countup">34,177</span></p>
                </div>
                <div class="mc-count-item">
                    <h4>Tuition Paid</h4>
                    <p>$ <span class="countup">793,361,890</span></p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="before-footer-link">
                    <a href="#" class="mc-btn btn-style-2">Become a member</a>
                    <a href="#" class="mc-btn btn-style-1">Become a teacher</a>
                </div>
            </div>
        </div>
    </div>
</section>