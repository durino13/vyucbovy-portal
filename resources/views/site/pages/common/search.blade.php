<section id="after-slider" class="after-slider section">
    <div class="awe-color bg-color-1"></div>
    <div class="after-slider-bg-2"></div>
    <div class="container">

        <div class="after-slider-content tb">
            <div class="inner tb-cell">
                <h4>Vyber si kurz</h4>
                <div class="course-keyword">
                    <input type="text" placeholder="Kľúčové slovo">
                </div>
                <div class="mc-select-wrap">
                    <div class="mc-select">
                        <select class="select" name="" id="all-categories">
                            <option value="" selected>Všetky predmety</option>
                            <option value="">2</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="tb-cell text-right">
                <div class="form-actions">
                    <input type="submit" value="Vyhľadaj" class="mc-btn btn-style-1">
                </div>
            </div>
        </div>

    </div>
</section>