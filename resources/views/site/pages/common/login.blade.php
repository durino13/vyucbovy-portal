<!-- LOGIN -->
<section id="login-content" class="login-content">
    <div class="awe-parallax bg-login-content"></div>
    <div class="awe-overlay"></div>
    <div class="container">
        <div class="row">

            <!-- FORM -->
            <div class="col-xs-12 col-lg-4 pull-right">
                <div class="form-login">

                    <h2 class="text-uppercase">Prihlásenie</h2>

                    <a class="loginBtn loginBtn--facebook btn-block" href="/auth/facebook">Login with Facebook</a>

                    <form action="/login" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="isUserLogin" value="true">
                        <h2 class="text-uppercase">-- or --</h2>
                        <div class="form-email">
                            <input name="email" type="text" placeholder="Email">
                        </div>
                        <div class="form-password">
                            <input name="password" type="password" placeholder="Password">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" id="check">
                            <label for="check">
                                <i class="icon md-check-2"></i>
                                Zapamätať</label>
                            <a href="{{ route('forgot.password.index') }}">Zabudli ste heslo?</a>
                        </div>
                        <div class="form-submit-1">
                            <input type="submit" value="Sign In" class="mc-btn btn-style-1">
                        </div>
                        <div class="link">
                            <a href="{{ route('register.index') }}">
                                <i class="icon md-arrow-right"></i>Ešte nemáte vytvorený účet? Nový účet
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END / FORM -->

            <div class="image">
                <img src="{{ asset('megacourse/images/homeslider/img-thumb.png') }}" alt="">
            </div>

        </div>
    </div>
</section>
<!-- END / LOGIN -->