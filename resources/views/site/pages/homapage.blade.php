@extends('site.layouts.main')

@section('content-top')

    <!-- PRELOADER -->
    @include('site.pages.common.preloader')
    <!-- END / PRELOADER -->

    <!-- HEADER -->
    @include('site.pages.common.header')
    <!-- END / HEADER -->

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if($errors)
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
    @endif

    <!-- HOME SLIDER -->
    @if(Auth::check())
        @include('site.pages.common.slider')
    @else
        @include('site.pages.common.login')
    @endif
    <!-- END / HOME SLIDER -->

    <!-- AFTER SLIDER -->
    @include('site.pages.common.search')
    <!-- END / AFTER SLIDER -->

    <!-- SECTION 1 -->
    @include('site.pages.common.intro')
    <!-- END / SECTION 1 -->

    <!-- SECTION 2 -->
    @include('site.pages.common.intro_2')
    <!-- END / SECTION 2 -->

    <!-- SECTION 3 -->
    @include('site.pages.common.recent_videos')
    <!-- END / SECTION 3 -->

    <!-- BEFORE FOOTER -->
    @include('site.pages.common.before-footer')
    <!-- END / BEFORE FOOTER -->

    <!-- FOOTER -->
    @include('site.pages.common.footer')
    <!-- END / FOOTER -->

@endsection