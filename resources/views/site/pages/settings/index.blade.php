@extends('site.layouts.two-columns')

@section('menu')

@endsection

@section('header')

    <section class="flex-container math">
        <h1 class="math__header">Nastavenia</h1>
    </section>

@endsection

@section('content')

    <div class="avatar-acount">
        <div class="changes-avatar">
            <div class="img-acount">
                <img src="{{ asset('megacourse/images/team-13.jpg') }}" alt="">
            </div>
            <div class="choses-file up-file">
                <input type="file">
                <input type="hidden">
                <a href="" class="mc-btn btn-style-6">Zmeniť obrázok</a>
            </div>
        </div>
        <div class="info-acount">
            <h3 class="md black">Anna Molly</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
            <div class="security">
                <div class="tittle-security">
                    <h5>Email</h5>
                    <input type="text">
                    <h5>Password</h5>
                    <input type="password" placeholder="Current password">
                    <input type="password" placeholder="New password">
                    <input type="password" placeholder="Confirm password">
                </div>
            </div>
        </div>
        <div class="input-save">
            <input type="submit" value="Save changes" class="mc-btn btn-style-1">
        </div>
    </div>

@endsection