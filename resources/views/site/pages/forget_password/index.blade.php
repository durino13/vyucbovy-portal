@extends('site.layouts.one-column-no-header')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <form class="form box-shadow" action="{{ route('forgot.password.email') }}" method="post">

                {{ csrf_field() }}

                <div class="form-header">
                    <h2>Zabudnuté heslo</h2>
                </div>

                <div class="form-content">
                    <ul>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Email</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input name="email" type="email">
                                        <span>Zadajte emailovú adresu, kde bude zaslané nové heslo</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
                                Odoslať
                            </button>
                        </div>

                    </ul>
                </div>

            </form>

        </div>
    </div>

@endsection