@extends('site.layouts.one-column-no-header')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <form class="form box-shadow" action="{{ route('forgot.password.reset.password') }}" method="post">

                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-header">
                    <h2>Reset hesla</h2>
                </div>

                <div class="form-content">
                    <ul>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Email</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input name="email" type="email">
                                        <span>Zadajte e-mail, pre ktorý chcete nastaviť nové heslo</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Nové heslo</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input name="password" type="password">
                                        <span>Zadajte nové heslo. Musí obsahovať aspoň 8 znakov</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Zopakuj heslo</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input name="password_confirmation" type="password">
                                        <span>Zopakuje heslo</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row-padding">
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
                                Nastaviť nové heslo
                            </button>
                        </div>

                    </ul>
                </div>

            </form>

        </div>
    </div>

@endsection