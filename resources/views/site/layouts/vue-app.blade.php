@extends('site.layouts.main')

@section('content-top')

    <!-- PRELOADER -->
    @include('site.pages.common.preloader')
    <!-- END / PRELOADER -->

    <!-- HEADER -->
    @include('site.pages.common.header')
    <!-- END / HEADER -->

    @if($errors)
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
    @endif

    <!-- HOME SLIDER -->
    @yield('header')

    @yield('content-main')

    <!-- BEFORE FOOTER -->
    @include('site.pages.common.before-footer')
    <!-- END / BEFORE FOOTER -->

    <!-- FOOTER -->
    @include('site.pages.common.footer')
    <!-- END / FOOTER -->

@endsection