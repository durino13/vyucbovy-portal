<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta id="_token" value="{{ csrf_token() }}">
    <!-- Google font -->
    {{--<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>--}}
    {{--<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>--}}
    <!-- Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('megacourse/css/library/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('megacourse/css/library/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('megacourse/css/library/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('megacourse/css/md-font.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('megacourse/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/site.all.css') }}">

    <!--[if lt IE 9]>
    <script src="{{ asset('http://html5shim.googlecode.com/svn/trunk/html5.js') }}"></script>
    <script src="{{ asset('http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js') }}"></script>
    <![endif]-->
    <title>EWB Online akadémia</title>
</head>
<body id="page-top" class="home">

<!-- PAGE WRAP -->
<div id="page-wrap">

    @yield('content-top')

</div>
<!-- END / PAGE WRAP -->

<?php

/*
 * Here we will read the filename from stats.json, because our filenames contain file hash and the file names
 * will differ ..
 */

$json_contents = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/assets/manifest.json'), true);
$site_bundle_name = $json_contents['/assets/site.js'];

?>

<!-- Load jQuery -->
<script type="text/javascript" src="{{ asset('megacourse/js/library/jquery-1.11.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('megacourse/js/library/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('megacourse/js/library/jquery.owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('megacourse/js/library/jquery.appear.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('megacourse/js/library/perfect-scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('megacourse/js/library/jquery.easing.min.js') }}"></script>
{{--TODO Install with npm --}}

<script type="text/javascript" src="{{ asset('megacourse/js/scripts.js') }}"></script>
<script type="text/javascript" src="{{ $site_bundle_name }}"></script>
</body>
</html>