@extends('site.layouts.no-header')

@section('content-main')

<section>
    @yield('content')
</section>

@endsection