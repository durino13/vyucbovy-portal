@extends('site.layouts.vue-app')

@section('content-main')

<section id="categories-content" class="categories-content">
    <div class="container">
        <div class="row">

            <div class="col-md-9 col-md-push-3">
                <div class="content grid">
                    <div class="row">

                        @yield('content')

                    </div>
                </div>
            </div>

            <!-- SIDEBAR CATEGORIES -->
            <div class="col-md-3 col-md-pull-9">
                <aside class="sidebar-categories">
                    <div class="inner">

                        @yield('menu')

                    </div>
                </aside>
            </div>
            <!-- END / SIDEBAR CATEGORIES -->

        </div>
    </div>
</section>

@endsection