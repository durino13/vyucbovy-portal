import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './lector/routes.js';
import VeeValidate from 'vee-validate';

// Bus
window.bus = new Vue()

// Init Vue router ..
const router = new VueRouter({
    routes,
    linkActiveClass: 'active'
})

// Register the router ..
Vue.use(VueRouter);
Vue.use(VeeValidate);

// Create and mount the vue application
new Vue({
    router
}).$mount('#lector-app')

// Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');