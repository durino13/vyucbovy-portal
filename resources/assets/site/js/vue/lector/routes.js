import CourseMenu from './CourseMenu.vue';
import NavBar from './NavBar.vue';
import CourseContainer from './CourseContainer.vue';

export default [

    // Edituj kurz
    {
        path: '/',
        components: {
            breadcrumbs: NavBar,
            leftMenu: CourseMenu,
            rightContent: CourseContainer
        }
    },
    {
        path: '/kurzy/:course',
        components: {
            breadcrumbs: NavBar,
            leftMenu: CourseMenu,
            rightContent: CourseContainer
        }
    },
    {
        path: '/kurzy/:course/lessons',
        components: {
            breadcrumbs: NavBar,
            leftMenu: CourseMenu,
            rightContent: CourseContainer
        }
    },

    // // Edituj lekciu
    // {
    //     path: '/lekcia/:lesson',
    //     components: {
    //         breadcrumbs: NavBar,
    //         leftMenu: CourseMenu,
    //         rightContent: CourseMenu
    //     }
    // }
]