$(document).ready(function() {

    // Ajax setup ..
    ajaxSetup();

    //----------------------------------------------------------
    // Bind events
    //----------------------------------------------------------

    $('.list-account').on('click', '#logout', function(e) {
        e.preventDefault();
        logout();
    })

});

//----------------------------------------------------------
// Helper functions
//----------------------------------------------------------

function ajaxSetup() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('#_token').attr('value')
        }
    });
}

function logout() {
    $.ajax('/logout', {
        method: 'post'
    }).done(function() {
        redirect('/');
    }).fail(function() {
        alert('An error occured while logging out ...');
    })
}

function redirect(url) {
    window.location.href = url;
}

