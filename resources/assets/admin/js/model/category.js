// Trash the article ..

class Category {

    /**
     * Delete the category
     * @returns {*}
     */
    static delete(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        return $.ajax({
            url: JSON.parse(general_baseURL)+'/administrator/category/'+ id,
            type: 'DELETE'
        });
    }

}

export default Category;
