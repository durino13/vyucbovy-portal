// Trash the article ..

class User {


    // TODO View article aj user maju rovnake IDcka, nemoze byt ..


    /**
     * Save the article
     * @param type Use POST to create NEW article, use PUT to update an existing article
     * @param id ID of an article you want to update
     * @returns {*}
     */
    static save(type = 'POST', id = '') {

        let url = JSON.parse(general_baseURL)+'/administrator/user';
        (type === 'PUT') ? url += '/'+id : '';

        // Save the tinyMCE content before we post it .. tinymce is a global variable ..
        tinymce.triggerSave();

        return $.ajax({
            url: url,
            type: type,
            data: $('#user-form').serialize()
        })
    }

    /**
     * Update an existing article
     * @param id ID of an article you want to update
     * @returns {*}
     */
    static update(id) {
        return User.save('PUT', id)
    }

    /**
     * Delete the category
     * @returns {*}
     */
    static delete(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        return $.ajax({
            url: JSON.parse(general_baseURL)+'/administrator/user/'+ id,
            type: 'DELETE'
        });
    }

}

export default User;
