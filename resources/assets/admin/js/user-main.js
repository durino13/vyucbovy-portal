import General from './form/general';
import UserForm from './form/user';

// ------------------------------------------------------------------------------------
// General stuff
// ------------------------------------------------------------------------------------

$(document).ready(function() {
    // Initialize the application ...
    General.init();
    UserForm.init();
});

// ------------------------------------------------------------------------------------
// Version number
// ------------------------------------------------------------------------------------

var package_json = require('json-loader!../../../../package.json');
$('#version').html(package_json.version);