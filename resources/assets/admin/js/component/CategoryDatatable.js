import Datatable from './Datatable';
import Category from '../model/category';
import Common from '../common';

var alertify = require("imports-loader?this=>window!alertify.js/dist/js/alertify.js");
require("imports-loader?this=>window!alertify.js/dist/css/alertify.css");

class CategoryDatatable extends Datatable {

    constructor(selector, trash, archive, recycle) {
        super(selector, trash, archive, recycle);
    }

    /**
     * Init buttons
     */
    initButtons() {

        return [
            {
                text: '<i class="fa fa-trash" aria-hidden="true"></i>',
                enabled: false,
                className: 'trashButton',
                action: function ( e, dt, node, config ) {

                    let rows = dt.rows('.selected');
                    let ids = rows.ids().toArray();

                    alertify.confirm("Do you really want to delete "+ ids.length +" selected item(s)?", function () {

                        for (let id of ids) {
                            Category.delete(id)
                                .done(function(response) {
                                    if (response.result) {
                                        // Remove selected rows ..
                                        rows.remove().draw();

                                        // Display the notification ..
                                        Common.notify('success', 'The category has been successfully deleted!');
                                    } else {
                                        Common.notify('error', 'The category can not be deleted, because there are '+
                                            response.articleCount+' articles assigned into this cateogry.');
                                    }
                                });
                        }

                    });

                }
            }
        ]

    }

}

export default CategoryDatatable;