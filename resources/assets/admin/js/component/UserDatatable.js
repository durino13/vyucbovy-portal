import Datatable from './Datatable';
import User from '../model/user';
import Common from '../common';

var alertify = require("imports-loader?this=>window!alertify.js/dist/js/alertify.js");
require("imports-loader?this=>window!alertify.js/dist/css/alertify.css");

class UserDatatable extends Datatable {

    constructor(selector, trash, archive, recycle) {
        super(selector, trash, archive, recycle);
    }

    /**
     * Init buttons
     */
    initButtons() {

        return [
            {
                text: '<i class="fa fa-trash" aria-hidden="true"></i>',
                enabled: false,
                className: 'trashButton',
                action: function ( e, dt, node, config ) {

                    let rows = dt.rows('.selected');
                    let ids = rows.ids().toArray();

                    alertify.confirm("Do you really want to delete "+ ids.length +" selected item(s)?", function () {

                        for (let id of ids) {
                            User.delete(id)
                                .done(function(response) {
                                    if (response.result) {
                                        // Remove selected rows ..
                                        rows.remove().draw();

                                        // Display the notification ..
                                        Common.notify('success', 'The user(s) has been successfully deleted!');
                                    }
                                });
                        }

                    });

                }
            }
        ]

    }

}

export default UserDatatable;