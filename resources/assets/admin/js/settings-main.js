import General from './form/general';
import SettingsForm from './form/settings';

// ------------------------------------------------------------------------------------
// General stuff
// ------------------------------------------------------------------------------------

$(document).ready(function() {
    // Initialize the application ...
    General.init();
    SettingsForm.init();
});

// ------------------------------------------------------------------------------------
// Version number
// ------------------------------------------------------------------------------------

var package_json = require('json-loader!../../../../package.json');
$('#version').html(package_json.version);