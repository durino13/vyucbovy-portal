import Common from '../common';
import User from '../model/user';

class UserForm {

    static init() {
        this.bindChosen();
        this.bindButtons();
    }

    /**************************************************************************
     * Helper form actions
     **************************************************************************/

    /**
     * Check, if the article is new
     */
    static isNew() {
        return $('input[name="user_id"]').val() === '' ? true : false;
    }

    static getUserID() {
        return $('input[name="user_id"]').val();
    }

    /**************************************************************************
     * Bind form events here ..
     **************************************************************************/

    static bindChosen() {
        $("#roles").chosen({width:"100%"});
    }

    static bindButtons() {

        // Save button
        $('#user_save').on('click', function(e) {
            e.preventDefault();
            User.update(UserForm.getUserID())
                .done(function() {
                    // Show the notification
                    Common.notify('success','The user form has been successfully saved!');
                })
        });

        // Save and close button
        $('#user_save_and_close').on('click', function(e) {
            e.preventDefault();

            if (UserForm.isNew()) {
                User.save()
                    .done(function() {
                        // Show the notification
                        Common.redirect(JSON.parse(general_baseURL)+'/administrator/user');
                        Common.notify('success', 'The user has been successfully created!');

                    })
            } else {
                User.update(UserForm.getUserID())
                    .done(function() {
                        // Show the notification
                        Common.redirect(JSON.parse(general_baseURL)+'/administrator/user');
                        Common.notify('success', 'The user has been successfully saved!');
                    })
            }

        });

    }

}

export default UserForm;