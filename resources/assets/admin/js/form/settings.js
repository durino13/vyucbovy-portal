import Common from '../common';
import Settings from '../model/settings';

class SettingsForm {

    static init() {
        this.bindButtons();
    }

    /**************************************************************************
     * Helper form actions
     **************************************************************************/

    /**
     * Check, if the article is new
     */
    static isNew() {
        return $('input[name="settings_id"]').val() === '' ? true : false;
    }

    static getSettingsID() {
        return $('input[name="settings_id"]').val();
    }

    /**************************************************************************
     * Bind form events here ..
     **************************************************************************/

    // static bindChosen() {
    //     $("#roles").chosen({width:"100%"});
    // }

    static bindButtons() {

        // Save button
        $('#settings_save').on('click', (e) => {
            console.log('Saving settings form ...')
            e.preventDefault();
            Settings.update(SettingsForm.getSettingsID())
                .done(() => {
                    // Show the notification
                    Common.notify('success','The settings form has been successfully saved!');
                })
        });

        // Save and close button
        $('#settings_save_and_close').on('click', (e) => {
            e.preventDefault();

            if (SettingsForm.isNew()) {
                Settings.save()
                    .done(() => {
                        // Show the notification
                        Common.redirect(JSON.parse(general_baseURL)+'/administrator/settings');
                        Common.notify('success', 'The settings has been successfully created!');

                    })
            } else {
                Settings.update(SettingsForm.getSettingsID())
                    .done(function() {
                        // Show the notification
                        Common.redirect(JSON.parse(general_baseURL)+'/administrator/settings');
                        Common.notify('success', 'The settings has been successfully saved!');
                    })
            }

        });

    }

}

export default SettingsForm;