import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import state from './state';
import config from '../app.config.js';
import dbService from '../services/db.service';

Vue.use(Vuex);

export default new Vuex.Store({
    state,
    actions: {

        // ---------------------------------------------------------------------
        // General
        // ---------------------------------------------------------------------

        // TODO currentRoute.subject a ostatne su undefined .. Treba fixnut .
        // TODO Vyhodit zvacsovanie pismen pri nacitani formulara, lezie to na nervy

        loadData(ctx, route) {
            return axios.all([
                    dbService.loadSubjects(),
                    dbService.loadCourses(route.subject),
                    dbService.loadModules(route.course),
                    dbService.loadLessons(route.module)
                ])
                .then(axios.spread((subjects, courses, modules, lessons) => {
                    ctx.commit('setSubjects', { subjects: subjects.data.data });
                    ctx.commit('setCourses', { courses: courses.data.data });
                    ctx.commit('setModules', { modules: modules.data.data });
                    ctx.commit('setLessons', { lessons: lessons.data.data });
                }, (err) => {
                    alert(err);
                }));
        },

        setIsDataLoaded(ctx, status) {
            ctx.commit('setIsDataLoaded', status);
        },

        setApplicationMode(ctx, mode) {
            ctx.commit('setApplicationMode', mode);
        },

        //----------------------------------------------------------
        // Section methods
        //----------------------------------------------------------

        loadSections(ctx, lessonIdentifier) {
            return dbService.loadSections(lessonIdentifier).then((response) => {
                if (response.data.result) {
                    ctx.commit('setLessonSections', response.data.data);
                }
            })
        },

        setActiveSection(ctx, sectionIdentifier) {

            let section = null;

            if (sectionIdentifier === 'create-video-section') {
                section = {
                    name: '',
                    description: '',
                    url: ''
                }
            } else {
                section = _.first(_.filter(ctx.state.sections, (section) => {
                    return section.identifier === sectionIdentifier;
                }));
            }

            ctx.commit('setActiveSection', section);
        },

        //----------------------------------------------------------
        // Subject actions
        //----------------------------------------------------------

        setActiveSubject(ctx, subjectIdentifier) {

            let subject = _.first(_.filter(ctx.state.subjects, (subject) => {
                return subject.identifier === subjectIdentifier;
            }));

            if (subject === undefined) {

                // TODO This object initialization is awful .. Refactor also empty objects in this file like this ..
                // Course defaults ..
                subject = {
                    name: '',
                    description: '',
                    identifier: ''
                }

            }

            ctx.commit('setActiveSubject', subject);
        },

        isNewSubjectForm(ctx, formState) {
            ctx.commit('isNewSubjectForm', formState);
        },

        //----------------------------------------------------------
        // Course actions
        //----------------------------------------------------------

        setActiveCourse(ctx, courseIdentifier) {

            let course = _.first(_.filter(ctx.state.courses, (course) => {
                return course.identifier === courseIdentifier;
            }));

            if (course === undefined) {

                // Course defaults ..
                course = {
                    name: '',
                    description: '',
                    identifier: '',
                    price: '',
                    is_archived: 0,
                    is_published: 0,
                    is_free_of_charge: 0
                }

            }

            ctx.commit('setActiveCourse', course);
        },

        deleteCourse(ctx, course) {
            ctx.commit('deleteCourse', course);
        },

        isNewCourseForm(ctx, formState) {
            ctx.commit('isNewCourseForm', formState);
        },

        //----------------------------------------------------------
        // Module actions
        //----------------------------------------------------------

        setActiveModule(ctx, module) {

            if (module === undefined) {

                // Module defaults ..
                module = {
                    name: '',
                    description: '',
                    identifier: '',
                    price: ''
                }

            }

            ctx.commit('setActiveModule', module);
        },

        //----------------------------------------------------------
        // Lesson actions
        //----------------------------------------------------------

        setActiveLesson(ctx, lessonIdentifier) {

            let lesson = _.first(_.filter(ctx.state.lessons, (lesson) => {
                return lesson.identifier === lessonIdentifier;
            }));

            if (lesson === undefined) {

                // Lesson defaults ..
                lesson = {
                    name: '',
                    description: '',
                    identifier: '',
                    price: ''
                }

            }

            ctx.commit('setActiveLesson', lesson);
        }
        
    },

    mutations: {

        setSubjects(state, { subjects }) {
            Vue.set(state, 'subjects', subjects);
        },

        setCourses(state, { courses }) {
            Vue.set(state, 'courses', courses);
        },

        setSubjects(state, { subjects }) {
            Vue.set(state, 'subjects', subjects);
        },

        setModules(state, { modules }) {
            Vue.set(state, 'modules', modules);
        },

        setLessons(state, { lessons }) {
            Vue.set(state, 'lessons', lessons);
        },

        setCourseModules(state, { modules }) {
            Vue.set(state, 'modules', modules);
        },

        setIsDataLoaded(state, status) {
            Vue.set(state, 'isDataLoaded', status);
        },

        setActiveSubject(state, subject) {
            state.activeSubject = subject;
        },

        setActiveCourse(state, course) {
            state.activeCourse = course;
        },

        setActiveModule(state, module) {
            state.activeModule = module;
        },

        setActiveLesson(state, lesson) {
            state.activeLesson = lesson;
        },

        setActiveSection(state, section) {
            Vue.set(state, 'activeSection', section);
        },

        setApplicationMode(state, mode) {
            // Available modes: 
            // 1. 'portal'
            // 2. 'lector'
            state.applicationMode = mode;
        },

        setLessonSections(state, sections) {
            Vue.set(state, 'sections', sections);
        },

        deleteCourse(state, courseToRemove) {
            let index = _.findIndex(state.courses, (course) => {
                return course.identifier === courseToRemove.identifier;
            });
            state.courses.splice(index, 1);
        },

        //----------------------------------------------------------
        // Subject mutations
        //----------------------------------------------------------

        isNewSubjectForm(state, formState) {
            state.isNewSubjectForm = formState;
        },

        //----------------------------------------------------------
        // Course mutations
        //----------------------------------------------------------

        isNewCourseForm(state, formState) {
            state.isNewCourseForm = formState;
        }

    },

    getters: {

        // ---------------------------------------------------------------------
        // Breadcrumbs
        // ---------------------------------------------------------------------

        getActiveModule(state) {
            return state.activeModule;
        },

        getUrlPrefix(state) {
            let prefix = '';
            state.applicationMode === config.APP_MODE.lector ? prefix = config.URL_PREFIX.lector : prefix = config.URL_PREFIX.portal;
            return prefix;
        }

    }

});
