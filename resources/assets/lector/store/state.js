export default {

    // portal, lektor
    applicationMode: '',

    // Do we have subjects, courses, modules and lessons loaded already?
    isDataLoaded: false,

    // List of all subjects in the database
    subjects: [],

    // Courses available in a specific subject
    courses: [],

    // List of courses available in a selected course
    modules: [],

    // List of lessons available in a specific module
    lessons: [],

    // Currently selected subject
    activeSubject: {},

    // Currently selected course
    activeCourse: {},

    // Currently selected module
    activeModule: {},

    // Currently selected lesson
    activeLesson: {},

    // Currently selected section
    activeSection: {},

    // Course form
    // isNewCourseForm: false,


};