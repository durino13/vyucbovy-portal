import axios from 'axios';
import qs from 'qs';

export default {

    //----------------------------------------------------------
    // Subject service methods
    //----------------------------------------------------------

    loadSubjects() {
        const url = '/api/subject';
        return axios.get(url);
    },

    // Save the course form
    save(subject, isNew) {

        let url = '';
        let method = '';

        if (isNew) {
            url = '/api/subject';
            method = 'post';
        } else {
            url = `/api/subject/${subject.identifier}`;
            method = 'put';
        }

        const data = {
            _method: method,
            name: subject.name,
            description: subject.description,
            identifier: subject.identifier,
            subject_id: subject.subject_id
        };

        // Metodu stringify pouzivam, aby axios vedel odoslat x-www-data-urlencoded Content-Type,
        // pretoze defaultne odosiela application/json
        return axios.post(url, qs.stringify(data));
    },

    // ---------------------------------------------------------------------------
    // Course service methods
    // ---------------------------------------------------------------------------    

    // Load all courses, or courses in a specific subject ..
    loadCourses(subjectIdentifier = null) {
        let url = '/api/course';
        subjectIdentifier === null ? url : url += `?subject=${subjectIdentifier}`;
        return axios.get(url);
    },

    // Save the course form
    saveCourse(subject, course, isNew) {

        let url = '';
        let method = '';

        if (isNew) {
            url = '/api/course';
            method = 'post';
        } else {
            url = `/api/course/${course.identifier}`;
            method = 'put';
        }

        let data = {
            _method: method,
            name: course.name,
            description: course.description,
            identifier: course.identifier,
            price: course.price,
            subject_id: subject.subject_id,
            duration: course.duration,
            difficulty_id: course.difficulty_id,
            is_published: course.is_published ? 1 : 0,
            is_archived: course.is_archived,
            is_free_of_charge: course.is_free_of_charge ? 1 : 0
        };

        // Metodu stringify pouzivam, aby axios vedel odoslat x-www-data-urlencoded Content-Type, 
        // pretoze defaultne odosiela application/json
        return axios.post(url, qs.stringify(data));
    },

    deleteCourse(course) {
        let url = `/api/course/${course.identifier}`;
        return axios.delete(url);
    },

    // ---------------------------------------------------------------------------
    // Module service methods
    // ---------------------------------------------------------------------------    

    loadModules(courseIdentifier = null) {
        let url = '/api/module';
        courseIdentifier === null ? url : url += `?course=${courseIdentifier}`;
        return axios.get(url);
    },

    // ---------------------------------------------------------------------------
    // Lesson service methods
    // ---------------------------------------------------------------------------    

    loadLessons(moduleIdentifier = null) {
        let url = '/api/lesson';
        moduleIdentifier === null ? url : url += `?module=${moduleIdentifier}`;
        return axios.get(url);
    },

    // Save the course form
    saveLesson(course, lesson, isNew) {

        let url = '';
        let method = '';

        if (isNew) {
            url = '/api/lesson';
            method = 'post';
        } else {
            url = `/api/lesson/${lesson.identifier}`;
            method = 'put';
        }

        let data = {
            _method: method,
            name: lesson.name,
            description: lesson.description,
            identifier: lesson.identifier,
            price: lesson.price,
            // course_id: course.course_id,
            is_published: lesson.is_published ? 1 : 0,
            is_archived: lesson.is_archived,
            is_free_of_charge: lesson.is_free_of_charge ? 1 : 0
        };

        // Metodu stringify pouzivam, aby axios vedel odoslat x-www-data-urlencoded Content-Type,
        // pretoze defaultne odosiela application/json
        return axios.post(url, qs.stringify(data));
    },

    // ---------------------------------------------------------------------------
    // Section service methods
    // ---------------------------------------------------------------------------

    loadSections(lessonIdentifier = null) {
        let url = '/api/section';
        lessonIdentifier === null ? url : url += `?lesson=${lessonIdentifier}`;
        return axios.get(url);
    },

    // ---------------------------------------------------------------------------
    // Vimeo service methods
    // ---------------------------------------------------------------------------

    async getAlbum(albumName = null) {
        let url = '/api/vimeo/albums?name=' + albumName;
        let result = await axios.get(url);
        return JSON.parse(result.data.data)[0];
    },

    async getVideosInAlbum(albumIdentifier) {
        let url = '/api/vimeo/albums/' + albumIdentifier + '/videos';
        let result = await axios.get(url);
        return JSON.parse(result.data.data);
    }

};
