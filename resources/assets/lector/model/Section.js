export class SectionVideo {

    static name() {
        return 'SectionVideo';
    }

    static createUrl() {
        return 'create-video-section';
    }

}

export class SectionContent {

    static name() {
        return 'SectionContent';
    }

    static createUrl() {
        return 'create-content-section';
    }

}

export class SectionAudio {

    static name() {
        return 'SectionAudio';
    }

    static createUrl() {
        return 'create-audio-section';
    }

}

export class SectionQuestionGroup {

    static name() {
        return 'SectionQuestionGroup';
    }

    static createUrl() {
        return 'create-question-group-section';
    }

}