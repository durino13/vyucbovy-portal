import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes.js';
import VeeValidate from 'vee-validate';
import Vuex from 'vuex';
import store from './store/store.js';

require('./scss/style.scss');
// require('./js/video.js');

const VueMaterial = require('vue-material');

// Bus
window.bus = new Vue();

// Init Vue router ..
const router = new VueRouter({
    routes,
    linkActiveClass: 'active'
});

// Register vue stuff
Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(Vuex);
Vue.use(VueMaterial);


Vue.material.registerTheme('default', {
    primary: 'blue',
    accent: 'red',
    warn: 'red',
    background: 'white'
});
Vue.material.setCurrentTheme('default');

// Create and mount the vue application
new Vue({
    router,
    store
}).$mount('#lector-app');

// Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');