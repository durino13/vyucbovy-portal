export default {

    APP_MODE: {
        portal: 'portal',
        lector: 'lector'
    },

    URL_PREFIX: {
        portal: '/',
        lector: '/lektor'
    }

};
