// Common components

import Navigation from './components/Navigation.vue';
import Breadcrumbs from './components/Common/Breadcrumbs.vue';
import ActionPanel from './components/Common/ActionPanel.vue';
import Header from './components/Header.vue';

// Layouts

import OnePaneLayout from './components/Layout/OnePaneLayout.vue';
import TwoPaneLayout from './components/Layout/TwoPaneLayout.vue';

// Subject

import SubjectFormCenter from './components/Subject/SubjectFormCenter.vue';
import SubjectFormStart from './components/Subject/SubjectFormStart.vue';

// Course 

import CourseFormEdit from './components/Course/CourseFormEdit.vue';
import CourseFormCenter from './components/Course/CourseFormCenter.vue';
import CourseLeftContent from './components/Course/CourseLeftContent.vue';
import CoursesList from './components/Course/CoursesList.vue';

// Module

import ModulesList from './components/Module/ModulesList.vue';
import LessonList from './components/Lesson/LessonList.vue';

// Lesson

import LessonFormContainer from './components/Lesson/LessonFormContainer.vue';

export default [

    //----------------------------------------------------------
    // Lector routes ..
    //----------------------------------------------------------

    {
        path: '/lektor',
        component: OnePaneLayout,
        children: [
            {
                name: 'show-subjects',
                path: '',
                components: {
                    navigation: Navigation,
                    header: Header,
                    action_panel: ActionPanel,
                    content: SubjectFormStart
                }
            },
            {
                name: 'subject-form-center',
                path: ':subject',
                components: {
                    navigation: Navigation,
                    header: Header,
                    action_panel: ActionPanel,
                    content: SubjectFormCenter
                }
            },
            {
                name: 'new-course',
                path: ':subject/novy-kurz',
                components: {
                    navigation: Navigation,
                    header: Header,
                    breadcrumbs: Breadcrumbs,
                    action_panel: ActionPanel,
                    content: CourseFormEdit
                }
            },
            {
                name: 'edit-course',
                path: ':subject/:course',
                components: {
                    navigation: Navigation,
                    header: Header,
                    breadcrumbs: Breadcrumbs,
                    action_panel: ActionPanel,
                    content: CourseFormEdit
                }
            },
            {
                name: 'edit-lesson',
                path: ':subject/:course/:lesson',
                components: {
                    navigation: Navigation,
                    header: Header,
                    action_panel: ActionPanel,
                    content: LessonFormContainer
                }
            },
            {
                name: 'edit-section',
                path: ':subject/:course/:lesson/:section',
                components: {
                    navigation: Navigation,
                    header: Header,
                    action_panel: ActionPanel,
                    content: LessonFormContainer
                }
            },
            {
                name: 'create-video-section',
                path: ':subject/:course/:lesson/create-video-section',
                components: {
                    navigation: Navigation,
                    header: Header,
                    action_panel: ActionPanel,
                    content: LessonFormContainer
                }
            }

        ]
    },

    //----------------------------------------------------------
    // Portal routes ..
    //----------------------------------------------------------

    // Courses screen
    {
        path: '/:subject',
        component: TwoPaneLayout,
        children: [
            {
                path: '',
                components: {
                    navigation: Navigation,
                    header: Header,
                    breadcrumbs: Breadcrumbs,
                    right_pane: CoursesList,
                }
            }
        ]
    },

    // Specific course screen

    {
        path: '/:subject/:course',
        component: TwoPaneLayout,
        children: [
            {
                path: '',
                components: {
                    navigation: Navigation,
                    header: Header,
                    breadcrumbs: Breadcrumbs,
                    left_pane: CourseLeftContent,
                    right_pane: ModulesList
                }
            }
        ]
    },

    // Module screen

    {
        path: '/:subject/:course/:module',
        component: TwoPaneLayout,
        children: [
            {
                path: '',
                components: {
                    navigation: Navigation,
                    header: Header,
                    breadcrumbs: Breadcrumbs,
                    left_pane: CourseLeftContent,
                    right_pane: LessonList
                }
            }
        ]
    },

    // TODO Lesson screen

    {
        path: '/:subject/:course/:module/:lesson',
        component: TwoPaneLayout,
        children: [
            {
                path: '',
                components: {
                    navigation: Navigation,
                    header: Header,
                    breadcrumbs: Breadcrumbs,
                    // left_pane: CourseLeftContent,
                    // right_pane: LessonList
                }
            }
        ]
    }

];
