Clone projektu:
-----------------------------------------------------
# git clone git@bitbucket.org:durino13/vyucbovy-portal.git

Rozbehnutie si development environmentu:
-----------------------------------------------------
# cd _dev
# docker-compose up
# docker exec -it dev_web_1 /bin/bash

Rozbehnutie projektu:
-----------------------------------------------------
# cd /var/www/html
# composer install
# cp .env.example .env
# php artisan key:generate
# Nastavit mysql pripojenie v .env subore
# php artisan migrate
# php artisan db:seed --class=DatabaseSeeder
# yarn install
# npm run build