<?php

namespace App;

use App\Lesson;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    protected $table = 'tbl_d_section';

    protected $primaryKey = 'section_id';

    //-----------------------------------------------------------------
    // Relations
    //-----------------------------------------------------------------

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lesson()
    {
        return $this->belongsTo('App\Lesson', 'lesson_id');
    }

    /**
     * We have several section types, so let's define a polymorphic relationship here ..
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function morph()
    {
        return $this->morphTo();
    }

    //-----------------------------------------------------------------
    // Methods
    //-----------------------------------------------------------------

    /**
     * Get the Section by it's identifier ..
     * @param $identifier
     * @return mixed
     */
    public static function getByIdentifier($identifier)
    {
        return self::with('section')->where('identifier', $identifier)->first();
    }

    /**
     * Find all sections in a specific lesson ..
     * @param Lesson $lesson
     * @return mixed
     */
    public static function getByLesson(Lesson $lesson)
    {
        return Section::with('morph')
            ->where('lesson_id', $lesson->lesson_id)
            ->get();
    }

}
