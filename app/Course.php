<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{

    use SoftDeletes;

    protected $table = 'tbl_d_course';

    protected $primaryKey = 'course_id';

    //-----------------------------------------------------------------
    // Casts
    //-----------------------------------------------------------------

    protected $casts = [
        'is_published' => 'boolean',
        'is_archived' => 'boolean',
        'is_free_of_charge' => 'boolean',
        'difficulty_id' => 'string'
    ];

    //-----------------------------------------------------------------
    // Appends
    //-----------------------------------------------------------------

    protected $appends = ['modules'];

    //-----------------------------------------------------------------
    // Relationships
    //-----------------------------------------------------------------

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lesson', 'course_id');
    }

    //-----------------------------------------------------------------
    // Accessor
    //-----------------------------------------------------------------

    /**
     * Ziskaj kolekciu unikatnych modulov v kurze ..
     * @return Collection
     */
    public function getModulesAttribute()
    {
        $modulesCollection = new Collection();

        $this->lessons()->each(function($lesson) use ($modulesCollection) {
            $modulesCollection->push($lesson->module()->with('lessons')->first());
        });

        return $modulesCollection->unique(function($module) {
            return $module->module_id;
        });

    }

    //-----------------------------------------------------------------
    // Methods
    //-----------------------------------------------------------------

    /**
     * Find all courses for subject
     * @param Subject $subject
     * @return mixed
     * @internal param int $subjectCode
     */
    public static function getBySubject(Subject $subject)
    {
        return Course::with('subject')
            ->where('subject_id', $subject->subject_id)
            ->get();
    }

    /**
     * Get the course by identifier
     * @param string|null $courseIdentifier
     * @return mixed
     * @throws \Exception
     */
    public static function getByIdentifier(string $courseIdentifier = null)
    {
        $course = Course::with(['lessons.module', 'subject'])
            ->where('identifier', $courseIdentifier)
            ->first();

        if (!$course) {
            throw new \Exception('Course not found in database');
        }

        return $course;
    }

}
