<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    protected $table = 'tbl_c_subject';

    protected $primaryKey = 'subject_id';

    public $timestamps = false;

    //-----------------------------------------------------------------
    // Relations
    //-----------------------------------------------------------------

    public function courses()
    {
        return $this->hasMany('App\Course', 'subject_id');
    }

    //-----------------------------------------------------------------
    // Methods
    //-----------------------------------------------------------------

    /**
     * Get the course by identifier
     * @param string|null $courseIdentifier
     * @return mixed
     * @throws \Exception
     */
    public static function getByIdentifier(string $subjectIdentifier = null)
    {
        $subject = Subject::with(['courses'])
            ->where('identifier', $subjectIdentifier)
            ->first();

        if (!$subject) {
            throw new \Exception('Subject not found in database');
        }

        return $subject;
    }

}
