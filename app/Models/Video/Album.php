<?php
/**
 * Created by PhpStorm.
 * User: marusiju
 * Date: 4.8.2017
 * Time: 22:33
 */

namespace App\Models\Video;


use JsonSerializable;

class Album implements JsonSerializable
{

    protected $uri;
    protected $identifier;
    protected $name;
    protected $description;
    protected $link;
    protected $duration;

    /**
     * Album constructor.
     * @param $uri
     * @param $name
     * @param $description
     * @param $link
     * @param $duration
     */
    public function __construct($uri, $name, $description, $link, $duration)
    {
        $this->uri = $uri;
        $this->identifier = $this->getIdentifier();
        $this->name = $name;
        $this->description = $description;
        $this->link = $link;
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * Format; /users/67982293/albums/4652988
     * @return string
     */
    public function getIdentifier(): string
    {
        $array = explode('/', $this->uri);
        return $array[4];
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        $array = explode('/', $this->uri);
        return $array[2];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }
}