<?php

namespace App\Models\Video;


interface IVideoApi
{
    public function sendHttpRequest(IEndpoint $endpoint, array $query);
}