<?php

namespace App\Models\Video;


class VimeoGetAllAlbumsEndpoint implements IEndpoint
{

    private $endpoint = '/me/albums';
    private $params = [];

    function __construct(array $params = null)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getEndpointUrl()
    {
        return $this->endpoint;
    }

}