<?php

namespace App\Models\Video;

interface IEndpoint
{
    public function getEndpointUrl();
    public function getParams();
}