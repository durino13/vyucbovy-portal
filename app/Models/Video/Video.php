<?php

namespace App\Models\Video;

use JsonSerializable;

class Video implements JsonSerializable
{

    protected $uri;
    protected $identifier;
    protected $name;
    protected $description;
    protected $link;
    protected $duration;
    protected $width;
    protected $height;
    protected $language;

    /**
     * Video constructor.
     * @param $uri
     * @param $identifier
     * @param $name
     * @param $description
     * @param $link
     * @param $duration
     * @param $width
     * @param $height
     * @param $language
     */
    public function __construct($uri, $name, $description, $link, $duration, $width, $height, $language)
    {
        $this->uri = $uri;
        $this->identifier = $this->getIdentifier();
        $this->name = $name;
        $this->description = $description;
        $this->link = $link;
        $this->duration = $duration;
        $this->width = $width;
        $this->height = $height;
        $this->language = $language;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        $array = explode('/', $this->uri);
        return $array[2];
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }
}