<?php

namespace App\Models\Video;


use App\Models\Video\IEndpoint;

class VimeoGetVideosInAlbumEndpoint implements IEndpoint
{
    private $endpoint;
    private $albumID;
    private $params;

    function __construct(string $albumID, array $params = null)
    {
        $this->albumID = $albumID;
        $this->params = $params;
        $this->endpoint = '/me/albums/'. $this->albumID .'/videos';
    }

    /**
     * URL of the endpoint
     * @return mixed
     */
    public function getEndpointUrl()
    {
        return $this->endpoint;
    }

    public function getParams()
    {
        return $this->params;
    }
}