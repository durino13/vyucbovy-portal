<?php

namespace App\Models\Video;

use App\VimeoDatabaseCacheProvider;
use Exception;
use Vinkla\Vimeo\Facades\Vimeo;

class VimeoApi implements IVideoApi
{

    private $connection;

    const GET_ALL_ALBUMS_CACHE_TYPE = 1;
    const GET_VIDEOS_IN_ALBUM_TYPE = 2;

    /**
     * VimeoApi constructor.
     * @param string|null $connection
     */
    public function __construct(string $connection = null) {
        if (!empty($connection)) {
            $this->connection = $connection;
        }

        $this->connection = 'development';
    }

    /**
     * Get albums from remote api
     * @param IEndpoint $endpoint
     * @param array $query
     * @return array
     * @throws Exception
     * @internal param $url
     */
    public function sendHttpRequest(IEndpoint $endpoint, array $query = null): array
    {
        $res = Vimeo::connection($this->connection)->request($endpoint->getEndpointUrl(), $query, 'GET');
        if ($this->receivedHttpSuccessResponse($res)) {
            return $res['body']['data'];
        }

        throw new Exception('Vimeo API call failed: '.$res['body']['error']);
    }

    /**
     * Get vimeo albums and leverage caching
     * @param array $query
     * @param VimeoDatabaseCacheProvider $cacheProvider
     * @return array|mixed
     */
    public function getAlbumsCached(array $query, VimeoDatabaseCacheProvider $cacheProvider)
    {

        $endpoint = new VimeoGetAllAlbumsEndpoint($query);

        // TODO Implement cache expiration
        if ($data = $cacheProvider->getResponse($endpoint)) {
            return [
                $data['response'],
                true    // result loaded from the cache
            ];
        }

        // If data is not in the cache, or invalid, call Vimeo api
        $data = $this->sendHttpRequest($endpoint, $query);

        $cacheProvider->cacheResponse($endpoint->getEndpointUrl(), $endpoint->getParams(), $data);

        return [
            $data,
            false   // not cached
        ];
    }

    /**
     * @param string $albumID
     * @param VimeoDatabaseCacheProvider $cacheProvider
     * @return array
     */
    public function getVideosInAlbumCached(string $albumID, VimeoDatabaseCacheProvider $cacheProvider)
    {

        $endpoint = new VimeoGetVideosInAlbumEndpoint($albumID);

        if ($data = $cacheProvider->getResponse($endpoint)) {
            return [
                $data['response'],
                true   // cached data
            ];
        }

        // Make the API call here
        $data = $this->sendHttpRequest($endpoint);

        // Store the response in the cache
        $cacheProvider->cacheResponse($endpoint->getEndpointUrl(), $endpoint->getParams(), $data);

        return [
            $data,
            false   // not cached
        ];
    }

    /**
     * @param $res
     * @return bool
     */
    private function receivedHttpSuccessResponse($res)
    {
        if ($res && isset($res['status']) && ($res['status'] >= 200 && $res['status'] < 300)) {
            return true;
        }

        return false;
    }

}