<?php

namespace App;

use App\Models\Video\IEndpoint;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class VimeoDatabaseCacheProvider extends Model
{

    protected $table = 'tbl_d_vimeo_cache';

    protected $primaryKey = 'vimeo_cache_id';

    //-----------------------------------------------------------------
    // Methods
    //-----------------------------------------------------------------

    /**
     * Cache data in the database
     * @param $url
     * @param $data
     * @internal param $type
     */
    public function cacheResponse($url, $params, $data)
    {
        $this->url = $url;
        $this->params = json_encode($params);
        $this->response = json_encode($data);
        $this->expires = new DateTime('now +' . $this->getCacheValidityDuration() . ' seconds');
        $this->save();
    }

    /**
     * Get the course by identifier
     * @param IEndpoint $endpoint
     * @return mixed
     * @internal param null|string $courseIdentifier
     */
    public function getResponse(IEndpoint $endpoint)
    {

        $data = VimeoDatabaseCacheProvider::where('url', $endpoint->getEndpointUrl())
            ->where('params', json_encode($endpoint->getParams()))
            ->first();

        if (!$data) {
            return false;
        }

        if (!$this->isCacheValid($data->expires)) {
            // TODO Delete cache entry, because it's not valid anymore
            // TODO Actually, I should do this one level up
        }

        return $data;
    }

    /**
     * @param $expires
     * @return DateTime
     * @internal param $data
     */
    public function isCacheValid($expires)
    {
        $now = new DateTime();
        $expires = new DateTime($expires);
        return $now < $expires;
    }

    /**
     * Defines the cache expiration duration in seconds
     * @return int
     */
    public function getCacheValidityDuration()
    {
        $expiration = (int)Settings::where('key', 'vimeo_cache_duration')->get()->first();

        if (empty($expiration)) {
            return 86400;
        }

        return $expiration->value;
    }



}
