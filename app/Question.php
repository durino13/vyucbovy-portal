<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $table = 'tbl_d_question';

    protected $primaryKey = 'question_id';

    public function group()
    {
        return $this->belongsTo(SectionQuestionGroup::class, 'section_question_group_id');
    }

}
