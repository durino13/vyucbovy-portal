<?php

namespace App;

use App\Interfaces\ITrashable;
use App\Http\Traits\Auth\Roles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements ITrashable
{

    protected $table = 'tbl_d_user';

    protected $primaryKey = 'user_id';

    use SoftDeletes;
    use CanResetPassword;
    use Notifiable;
    use Roles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'status_id', 'avatar', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /******************************************************************************
     * Relations
     ******************************************************************************/

    public function socialProviders()
    {
        return $this->hasMany('App\SocialProvider', 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'tbl_r_user_role');
    }

    /******************************************************************************
     * Trash methods
     ******************************************************************************/

    public function trashTitle()
    {
        return $this->first_name;
    }

    public function trashDocumentType()
    {
        return 'User';
    }

    public function trashedAt()
    {
        $this->deleted_at;
    }

    /******************************************************************************
     * Model methods
     ******************************************************************************/

    public function getNameAttribute()
    {
        return $this->first_name. ' '. $this->last_name;
    }

    /******************************************************************************
     * Model methods
     ******************************************************************************/

    public function isAdmin()
    {
        // TODO Toto by uz fungovat nemalo .. Pouzivam role ..
        return Auth::user()->is_admin == 1;
    }

    public function isActiveStatus()
    {
        return Auth::user()->status_id == 10;
    }

    public function updateUser($user, UserRequest $request)
    {
        return  $user->update([
            'first_name'  => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'is_subscribed' => $request->is_subscribed,
            'is_admin' => $request->is_admin,
            'status_id' => $request->status_id,
        ]);
    }

    public function showAdminStatusOf($user)
    {
        return $user->is_admin ? 'Yes' : 'No';
    }

    public function showNewsletterStatusOf($user)
    {
        return $user->is_subscribed == 1 ? 'Yes' : 'No';
    }

    public function widgets()
    {
        return $this->hasMany('App\Widget');
    }

}
