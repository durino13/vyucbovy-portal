<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionVideo extends Model
{

    protected $table = 'tbl_d_section_video';

    protected $primaryKey = 'section_video_id';

    public function morph()
    {
        return $this->morphMany(Section::class, 'morph_id');
    }

}
