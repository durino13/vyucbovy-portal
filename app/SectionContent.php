<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionContent extends Model
{
    protected $table = 'tbl_d_section_content';

    protected $primaryKey = 'section_content_id';

    public function morph()
    {
        return $this->morphMany(Section::class, 'morph_id');
    }

}
