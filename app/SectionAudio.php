<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionAudio extends Model
{
    protected $table = 'tbl_d_section_audio';

    protected $primaryKey = 'section_audio_id';

    public function morph()
    {
        return $this->morphMany(Section::class, 'morph_id');
    }

}
