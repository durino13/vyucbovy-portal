<?php

namespace App;

use App\Lesson;
use Illuminate\Database\Eloquent\Model;

class SectionQuestionGroup extends Model
{

    // TODO Dorobit relaciu s Question
    // TODO Spravit metodu, ktora vrati section group 'with' question a tu potom posielat ajaxom pri nacitani sekcii ..

    protected $table = 'tbl_d_section_question_group';

    protected $primaryKey = 'section_question_group_id';

    public function questions()
    {
        return $this->hasMany(Question::class, 'question_id');
    }

}
