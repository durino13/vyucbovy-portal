<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $table = 'tbl_c_role';

    protected $primaryKey = 'role_id';

}
