<?php

namespace App\Http\Controllers\Lector;
use App\Http\Controllers\Controller;

/**
 * Created by PhpStorm.
 * User: marusiju
 * Date: 18.3.2017
 * Time: 15:45
 */
class LectorController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $errors = session()->get('errors');
        return view('lector.module-overview', compact('errors'));
    }

}