<?php

namespace App\Http\Controllers\Auth;

use App\Http\Traits\Auth\ManagesSocialAuth;
use App\Http\Traits\Auth\SyncsSocialUsers;
use App\User;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{

    use ManagesSocialAuth;

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller manages Facebook (or other) social provider logsins ..
    | If you want to see the normal login, check the LoginController instead ..
    |
    */

    /**
     * Check, if the user is active. Must have status_id = 10
     */
    protected function checkStatusLevel()
    {
        if ( ! Auth::user()->isActiveStatus()) {
            Auth::logout();
            throw new NoActiveAccountException;
        }
    }

}
