<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

class RegisterController extends Controller
{

    public function index()
    {
        $errors = session()->get('errors');
        return view('site.pages.register.index', compact('errors'));
    }

}
