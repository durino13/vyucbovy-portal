<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Site\Controller;

class UserController extends Controller
{

    /**
     * Zobraz stranku s nastaveniami pre uzivatela ..
     */
    public function index()
    {
        return view('site.pages.settings.index');
    }

}
