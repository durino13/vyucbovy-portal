<?php

namespace App\Http\Controllers\Site;

use DB;
use App\Subject;
use App\Article;
use Tracy\Debugger;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $errors = session()->get('errors');
        return view('site.pages.homapage', compact('errors'));
    }

    /**
     * Nova verzia stranky
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function nova()
    {
        $errors = session()->get('errors');
        return view('site.pages_v1.homepage.homapage', compact('errors'));
    }

    /**
     * Catch all rule, which finds an article based on it's alias
     *
     * @param $alias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($alias)
    {
        $article = Article::where('alias','=', $alias)->first();
        return view('site.layouts.detail', ['article' => $article]);
    }

    /**
     * Display the view for the lector
     */
    public function lector()
    {
        return view('site.pages.lector.math');
    }

    /******************************************************************************
     * Lector routes
     ******************************************************************************/

    /**
     * Route to math index page ..
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function math()
    {
        $courses = Course::all();
        $errors = session()->get('errors');

        return view('site.pages.math.index', compact('errors', 'courses'));
    }

    public function english()
    {
        $errors = session()->get('errors');
        return view('site.pages.english.index', compact('errors'));
    }

}
