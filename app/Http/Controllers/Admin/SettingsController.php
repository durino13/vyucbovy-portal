<?php

namespace App\Http\Controllers\Admin;

use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::all();
        return view('admin.settings.index', ['settings' => $settings ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key' => 'required',
            'value' => 'required',
            'type' => 'required'
        ]);

        $settings = new Settings();
        return $this->saveSettings($request, $settings);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $settings_id
     * @return \Illuminate\Http\Response
     */
    public function edit($settings_id)
    {
        $settings = Settings::findOrFail($settings_id);
        return view('admin.settings.edit', ['settings' => $settings]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'key' => 'required',
            'value' => 'required',
            'type' => 'required'
        ]);

        $settings = Settings::findOrFail($id);
        return $this->saveSettings($request, $settings);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function saveSettings(Request $request, $settings)
    {
        try {
            $settings->key = $request->input('key');
            $settings->value= $request->input('value');
            $settings->type = $request->input('type');
            $settings->save();
        } catch (Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        $request->session()->flash('status', 'Settings successfully saved.');

        if ($request->get('action') == 'save') {
            return redirect()->route('administrator.settings.edit', ['settings' => $settings]);
        } elseif ($request->get('action') == 'save_and_close') {
            return redirect()->route('administrator.settings.index');
        }
    }
}
