<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Lesson;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ModuleController extends Controller
{

    /**
     * Get the list of all modules in the database, or in a specific course
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $course = null;

        if ($request->has('course')) {
            $course = Course::getByIdentifier($request->input('course'));
            $modules = Module::getModulesInCourse($course);
        } else {
            $modules = Module::with('lessons')->get();
        }

        return response()->json([
            'result' => true,
            'data' => $modules
        ]);
    }

    //  /**
    //  * Get all modules in a specific course
    //  * @param Request $request
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function show(Request $request, $courseID)
    // {

    //     $course = Course::getByIdentifier($courseID);

    //     if (empty($course)) {
    //         return response()->json([
    //             'result' => false,
    //             'msg' => 'Model nebol najdeny ...'
    //         ]);
    //     }

    //     return response()->json([
    //         'result' => true,
    //         'data' => [
    //             'course' => $course
    //         ]
    //     ]);

    // }
    
}
