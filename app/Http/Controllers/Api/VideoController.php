<?php

namespace App\Http\Controllers;

use App\Models\Vimeo\Album;
use App\Models\Vimeo\Video;
use Illuminate\Http\Request;

/**
 * This class oposed to VimeoController, returns business classes hydrated with data ..
 * Class VideoController
 * @package App\Http\Controllers
 */
class VideoController extends Controller
{

    /**
     * @param string $name
     * @return array|mixed
     */
    public function getAlbumByName(Request $request, string $name): Album
    {

        $connection = $request->query('connection');
        $api = new VimeoApi($connection);
        $query = ['query' => $name];
        $endpoint = new VimeoGetAllAlbumsEndpoint($query);

        // If data is not in the cache, or invalid, call Vimeo api
        $data = $api->getAlbums($endpoint, $query);

        $album = new Album();

        return $album;

    }

}
