<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Lesson;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LessonController extends Controller
{

    /**
     * Get a list of lessons, if moduleId is specified in the parameter, filter only lessons for a specific module
     * http://ewb.local.d/api/lesson?moduleId=module-identifier
     * @param Route $route
     */
    public function index(Request $request)
    {

        if ($request->has('module')) {
            $module = Module::where('identifier','=',$request->input('module'))->first();
            $lessons = Lesson::getByModule($module);
        } else {
            $lessons = Lesson::all();
        }

        return response()->json([
            'result' => true,
            'data' => $lessons
        ]);

    }

    /**
     * Get the list of all courses in a given subject ..
     */
    public function getByCode(Request $request)
    {
        $lesson = $request->input('courseCode');
        return response()->json($subject->courses()->get());
    }

    /**
     * Update the course
     * @param Request $request
     * @param $course
     * @return \Illuminate\Http\JsonResponse
     * @internal param $courseId
     */
    public function update(Request $request, $lesson)
    {

        try {

            $lesson = Lesson::getByIdentifier($lesson);
            $lesson->identifier = $request->input('identifier');
            $lesson->name = $request->input('name');
            $lesson->description = $request->input('description');
            $lesson->author_id = 1; // TODO Hardcoded
            $lesson->is_published = $request->input('is_published');
            $lesson->is_archived = $request->input('is_archived');
            $lesson->is_free_of_charge = $request->input('is_free_of_charge');
            $lesson->save();

            // Return the successful response
            return response()->json([
                'result' => true,
                'data' => $lesson
            ]);

        } catch (Exception $e) {

            // Return the failure
            return response()->json([
                'result' => false,
                'data' => $e->getMessage()
            ]);

        }

    }
    
}
