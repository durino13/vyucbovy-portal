<?php

namespace App\Http\Controllers\Api;

use App\Models\Video\Album;
use App\Models\Video\Video;
use Illuminate\Http\Request;
use App\Models\Video\VimeoApi;
use App\VimeoDatabaseCacheProvider;
use App\Http\Controllers\Controller;

class VimeoController extends Controller
{

    /**
     * Get all available albums in a specific account
     * @param Request $request
     * @param string|null $albumName
     * @return array|mixed
     */
    public function getAlbums(Request $request): any
    {

        $albumName = $request->query('name');

        $albums = collect();

        $connection = $request->query('connection');
        $vimeo = new VimeoApi($connection);
        $cacheProvider = new VimeoDatabaseCacheProvider();

        $query = ['query' => $albumName];

        list($data, $isFromCache) = $vimeo->getAlbumsCached($query, $cacheProvider);

        if (!empty($data)) {

            $data = json_decode($data);

            foreach ($data as $albumStdObject) {

                // Create new album object
                $album = new Album(
                    $albumStdObject->uri,
                    $albumStdObject->name,
                    $albumStdObject->description,
                    $albumStdObject->link,
                    $albumStdObject->duration
                );

                // Push it to the collection
                $albums->push($album);

            }

            $this->echoResponseAndExit($albums->toJson(), $isFromCache);

        }

        $this->echoResponseAndExit('[]', $isFromCache);

    }


    /**
     * Get Videos in a specific album
     * @param Request $request
     * @param string $albumID
     * @return array|mixed
     */
    public function getVideosInAlbum(Request $request, string $albumID)
    {
        $connection = $request->query('connection');
        $api = new VimeoApi($connection);
        $cacheProvider = new VimeoDatabaseCacheProvider();

        // Make the API call here
        list($data, $isFromCache) = $api->getVideosInAlbumCached($albumID, $cacheProvider);

        $videoStdObject = json_decode($data);

        // Create new album object
        $video = new Video(
            $videoStdObject[0]->uri,
            $videoStdObject[0]->name,
            $videoStdObject[0]->description,
            $videoStdObject[0]->link,
            $videoStdObject[0]->duration,
            $videoStdObject[0]->width,
            $videoStdObject[0]->height,
            $videoStdObject[0]->language,
            $videoStdObject[0]->created_time,
            $videoStdObject[0]->modified_time,
            $videoStdObject[0]->release_time,
            $videoStdObject[0]->status,
            $videoStdObject[0]->resource_key
        );

        $this->echoResponseAndExit(json_encode($video), $isFromCache);

    }

    /**
     * Create a unitform response api object
     * @param bool $cached
     * @param string $data
     * @return array
     */
    private function echoResponseAndExit(string $data, bool $cached)
    {

        // TODO Find laravel way to send json response
        echo json_encode([
            'cached' => $cached,
            'data' => $data
        ]);
        exit;

    }

}
