<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Http\Controllers\Admin\Controller;
use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{

    /**
     * Get the list of all subjects (Math, English ...)
     */
    public function index(Request $request)
    {
        $subjects = Subject::with('courses')->get();
        return response()->json([
            'result' => true,
            'data' => $subjects
        ]);
    }

    /**
     * Update the subject
     * @param Request $request
     * @param $subjectIdentifier
     * @return \Illuminate\Http\JsonResponse
     * @internal param $courseId
     */
    public function update(Request $request, $subjectIdentifier)
    {
        try {

            $subject = Subject::getByIdentifier($subjectIdentifier);
            $subject->name = $request->input('name');
            $subject->description = $request->input('description');
            $subject->identifier = $request->input('identifier');
            $subject->save();

            // Return the successful result
            return response()->json([
                'result' => true,
                'data' => $subject
            ]);

        } catch (Exception $e) {

            // Return the error
            return response()->json([
                'result' => false,
                'data' => $e->getMessage()
            ]);
        }

    }
    
}
