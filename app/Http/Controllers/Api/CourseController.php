<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Admin\Controller;

class CourseController extends Controller
{

    /**
     * Get the list of all courses in a given subject or
     */
    public function index(Request $request)
    {
        $courses = [];

        if ($request->has('subject')) {
            $subject = Subject::where('identifier', '=', $request->input('subject'))->first();
            $courses = Course::getBySubject($subject);
        } else {
            $courses = Course::all();
        }

        return response()->json([
            'result' => true,
            'data' => $courses
        ]);

    }

    /**
     * Create a new course
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        try {
            $this->validate($request, [
                'identifier' => 'required',
                'name' => 'required',
                'description' => 'required'
            ]);
        } catch (ValidationException $e) {
            return response()->json($e->validator->errors(), 422);
        }

        try {

            $course = new Course();
            $course->identifier = $request->input('identifier');
            $course->name = $request->input('name');
            $course->description = $request->input('description');
            $course->price = $request->input('price');
            $course->duration = $request->input('duration');
            $course->difficulty_id = (string)$request->input('difficulty_id');
            $course->author_id = 1; // TODO Hardcoded
            $course->subject_id = $request->input('subject_id');
            $course->is_published = $request->input('is_published');
            $course->is_archived = $request->input('is_archived');
            $course->is_free_of_charge = $request->input('is_free_of_charge');
            $course->save();

            return response()->json([
                'result' => true,
                'data' => $course
            ]);

        } catch (QueryException $e) {

            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){

                // Return the failure
                return response()->json([
                    'result' => false,
                    'data' => $e->getMessage()
                ]);

            }

            // Return the failure
            return response()->json([
                'result' => false,
                'data' => $e->getMessage()
            ]);

        }

    }

    /**
     * Update the course
     * @param Request $request
     * @param $course
     * @return \Illuminate\Http\JsonResponse
     * @internal param $courseId
     */
    public function update(Request $request, $course)
    {

        try {

            $course = Course::getByIdentifier($course);
            $course->identifier = $request->input('identifier');
            $course->name = $request->input('name');
            $course->description = $request->input('description');
            $course->price = $request->input('price');
            $course->duration = $request->input('duration');
            $course->difficulty_id = (string)$request->input('difficulty_id');
            $course->author_id = 1; // TODO Hardcoded
            $course->subject_id = 1; // TODO Hardcoded
            $course->is_published = $request->input('is_published');
            $course->is_archived = $request->input('is_archived');
            $course->is_free_of_charge = $request->input('is_free_of_charge');
            $course->save();

            // Return the successful response
            return response()->json([
                'result' => true,
                'data' => $course
            ]);

        } catch (Exception $e) {

            // Return the failure
            return response()->json([
                'result' => false,
                'data' => $e->getMessage()
            ]);

        }

    }

    /**
     * Get course info
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $courseIdentifier)
    {

        $course = Course::getByIdentifier($courseIdentifier);

        if (empty($course)) {
            return response()->json([
                'result' => false,
                'msg' => 'Model nebol najdeny ...'
            ]);
        }

        return response()->json([
            'result' => true,
            'data' => [
                'course' => $course
            ]
        ]);

    }

    /**
     * @param Request $request
     * @param $courseIdentifier
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $courseIdentifier)
    {

        try {

            /* @var $course Course */
            $course = Course::getByIdentifier($courseIdentifier);
             $course->delete();

            return response()->json([
                'result' => true,
                'data' => [
                    'course' => $course
                ]
            ]);

        } catch (Exception $e) {

            return response()->json([
                'result' => false,
                'data' => [
                    'error' => $e->getMessage()
                ]
            ]);

        }

    }
    
}
