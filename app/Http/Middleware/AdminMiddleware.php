<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Only members of the Administrator group can access the administration ..
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        // Get the user
        $user = Auth::user();

        if (isset($user)) {
            if ($user->isRole('ADMIN')) {
                return $next($request);
            } else {
                return redirect()->route('index')->withErrors(['Only members of administrators group may access this URL']);
            }
        } else {
            return response('Unauthorized.', 401);
        }

    }
}
