<?php

namespace App\Http\Traits\Auth;

trait SetsAuthUser
{

    /**
     * @param $socialUser
     * @return User
     * @throws \App\Exceptions\CredentialsDoNotMatchException
     * @throws \App\Exceptions\EmailAlreadyInSystemException
     */

    private function setAuthUser($socialUser)
    {

        $authUser = $this->findOrCreateUser($socialUser);

        return $authUser;

    }

    private function setSocialUserName($socialUser)
    {

        switch ($this->provider){

            case 'facebook' :

                // General provider fields ..
                $this->userName = $socialUser->getName();

                // Facebook specific fields ..
                $this->firstName = $socialUser->user['first_name'];
                $this->lastName = $socialUser->user['last_name'];
                $this->gender = $socialUser->user['gender'];

                break;

            default :

                is_null($socialUser->name) ?

                    $this->userName = $this->createUserName() :

                    $this->userName = $socialUser->name;

                break;


        }




    }


    private function createUserName()
    {

        return  str_random(15);

    }


}