<?php

namespace App\Http\Traits\Auth;

use App\Role;
use App\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait Roles {

    public function roles()
    {
        return $this->hasMany('App\Models\Role');
    }


    //attach a role by name to a user
    public function attach($role_code)
    {
        try{
            $role = Role::where('code', '=', $role_code)->firstOrFail();

            //if the role already exists on a user, return false
            if($this->isRole($role->code)) {
                return false;
            }

            Permission::create([
                'user_id' => $this->id,
                'role_id' => $role->id,
            ]);

            return true;
        }
        catch(ModelNotFoundException $e) {
            return false;
        }
    }

    //check if a user is a certain role
    public function isRole($role_code)
    {
        try{
            $role = Role::where('code', '=', $role_code)->firstOrFail();
            Permission::where([
                'user_id' => $this->user_id,
                'role_id' => $role->role_id,
            ])->firstOrFail();

            return true;
        }
        catch(ModelNotFoundException $e) {
            return false;
        }
    }

}