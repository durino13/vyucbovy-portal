<?php

namespace App\Http\Traits\Auth;

use App\Exceptions\EmailAlreadyInSystemException;
use App\Exceptions\EmailNotProvidedException;
use App\SocialProvider;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Socialite;


trait ManagesSocialAuth
{

    // the traits contain the methods needed for the handleProviderCallback

    use FindsOrCreatesUsers,
        RoutesSocialUser,
        SetsAuthUser,
        SyncsSocialUsers,
        VerifiesSocialUsers;

    private $provider;

    private $userName;

    private $firstName;

    private $lastName;

    private $gender;

    private $approvedProviders = ['facebook'];


    public function handleProviderCallback($provider)
    {

        try {// the provider must be in the list of approved providers ..
            $this->verifyProvider($this->provider = $provider);

            // load social user (e.g. facebook user) and read it's email
            $socialUser = $this->getUserFromSocialite($provider);
            $providerEmail = $socialUser->getEmail();

            // make sure, email is defined. If not, throw an exception ..
            if ($this->socialUserHasNoEmail($providerEmail)) {
                throw new EmailNotProvidedException;
            }

            // initialize $this->username property as it can be different for different providers ..
            $this->setSocialUserName($socialUser);


            // normally, we should not login with facebook, if we are already logged in.
            // if the user is already logged in, try to sync it.
            if ($this->socialUserAlreadyLoggedIn()) {
                $this->checkIfAccountSyncedOrSync($socialUser);
            }

            try {
                // we try to pair social email address with email address from our database. If there's a match, we return the
                // user, if not, we create one.
                $authUser = $this->setAuthUser($socialUser);
            } catch (EmailAlreadyInSystemException $e) {
                return redirect()->route('index')->withErrors(['Email is already registered in the system. Login and sync first.']);
            }

            // user must have a record in our database .. If so, log the user in ..
            $this->loginAuthUser($authUser);

            $this->logoutIfUserNotActiveStatus();

            return $this->redirectUser();
        } catch (Exception $e) {
            // TODO pass the error ..
            return redirect()->route('index');
        }

    }

    private function syncUserAccountWithSocialData($socialUser)
    {

        // one last check to see if the social id already exists


        if ($this->socialIdAlreadyExists($socialUser)){

            throw new CredentialsDoNotMatchException;

        }

        // lookup user id and update create provider record


        SocialProvider::create([
            'user_id' => Auth::user()->user_id,
            'source'  => $this->provider,
            'source_id'  => $socialUser->id,
            'avatar'      => $socialUser->avatar
        ]);

    }

}
