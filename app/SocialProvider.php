<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialProvider extends Model
{

    protected $table = 'tbl_d_social_provider';

    protected $primaryKey = 'social_provider_id';

    protected $fillable = [
        'user_id',
        'source',
        'source_id',
        'avatar'];

    public function user()
    {

        return $this->belongsTo('App\User', 'user_id');

    }
}
