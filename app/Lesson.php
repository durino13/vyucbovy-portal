<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{

    protected $table = 'tbl_d_lesson';

    protected $primaryKey = 'lesson_id';

    //-----------------------------------------------------------------
    // Casts
    //-----------------------------------------------------------------

    protected $casts = [
        'is_published' => 'boolean',
        'is_archived' => 'boolean',
        'is_free_of_charge' => 'boolean'
    ];

    //-----------------------------------------------------------------
    // Relations
    //-----------------------------------------------------------------

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function module()
    {
        return $this->belongsTo('App\Module', 'module_id');
    }

    public function sections()
    {
        return $this->hasMany('App\Section', 'section_id');
    }


    //-----------------------------------------------------------------
    // Methods
    //-----------------------------------------------------------------

    /**
     * Get all the lessons in a specific course ..
     * @param Course $course
     * @return mixed
     * @internal param int $subjectCode
     */
    public static function getByCourse(Course $course = null)
    {
        return Lesson::where('course_id', $course->course_id)->get();
    }

    /**
     * Get all the lessons in a specific module
     * @param Module|null $module
     * @return mixed
     */
    public static function getByModule(Module $module = null)
    {
        return Lesson::where('module_id', $module->module_id)->get();
    }

    /**
     * Get the lesson by identifier
     * @param string|null $courseIdentifier
     * @return mixed
     * @throws \Exception
     */
    public static function getByIdentifier(string $lessonIdentifier = null)
    {
        $lesson = Lesson::where('identifier', $lessonIdentifier)
            ->first();

        if (!$lesson) {
            throw new \Exception('Lesson not found in database');
        }

        return $lesson;
    }

}
