<?php
/**
 * Created by PhpStorm.
 * User: marusiju
 * Date: 11.2.2017
 * Time: 21:20
 */

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    protected $table = 'tbl_d_module';

    protected $primaryKey = 'module_id';

    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'module_id');
    }

    /**
     * Return the modules in the course
     * @param Course $course
     */
    public static function getModulesInCourse(Course $course)
    {

        // Get all lessons in the course
        $lessons = Lesson::getByCourse($course);

        $modules = new Collection();

        // Get available lessonModuleIDs
        $lessons->map(function($lesson) use ($modules) {
            return $lesson->module_id;
        })->unique()->each(function($moduleID) use ($modules) {
            $modules->push(Module::with('lessons')->findOrFail($moduleID));
        });

        // Get it's names
        return $modules;

    }

}