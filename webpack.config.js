const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');


const extractSass = new ExtractTextPlugin('[name].all.sass');
const extractCss = new ExtractTextPlugin('[name].all.css');

module.exports = {
    entry: {
        site: './resources/assets/site/js/site.bundle.js',
        site_v1: './resources/assets/site/js/site_v1.bundle.js',
        lector: './resources/assets/lector/lector.bundle.js',
        admin: './resources/assets/admin/js/admin.bundle.js'
    },
    output: {
        path: 'public/assets',
        publicPath: '/assets/',
        filename: '[name].[chunkhash].all.js'
    },
    devtool: 'source-map',
    module: {
        loaders: [

            // Ecmascript 6 loader

            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },

            // Vue loader
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        // Since sass-loader (weirdly) has SCSS as its default parse
                        // mode, we map the "scss" and "sass" values for the lang 
                        // attribute to the right configs here. other preprocessors should 
                        // work out of the box, no loader config like this nessessary.
                        scss: 'vue-style-loader!css-loader!sass-loader',
                        sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                    }
                    // other vue-loader options go here
                }
            },

            // Css loaders
            {
                test: /\.(css|scss)$/i,
                loaders: extractCss.extract(['css-loader?sourceMap', 'sass-loader?sourceMap'])
            },

            // Picture loaders
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=[name].[ext]',
                    {
                        loader: 'image-webpack-loader',
                        query: {
                            gifsicle: {
                                interlaced: false
                            },
                            optipng: {
                                optimizationLevel: 4
                            },
                        }
                    }
                ]
            },

            // Font loaders
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader'
            },
            {
                test: /\.woff(\?.*)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.woff2(\?.*)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?.*)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?.*)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.svg(\?.*)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            },

            // TinyMCE shimming ..
            {
                test: require.resolve('tinymce/tinymce'),
                loader: [
                    'imports-loader?this=>window',
                    'exports-loader?window.tinymce'
                ]
            },
            {
                test: /tinymce\/(themes|plugins)\//,
                loader: 'imports-loader?this=>window'
            }

        ]
    },
    resolve: {
        alias: {
            vue$: 'vue/dist/vue.common.js'
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.$': 'jquery'
        }),
        extractSass,
        extractCss,
        // The manifest file will contain the names of filenames we need to call 
        // in the <script> tags in the <head>
        // These filenames will contain has, so we need to load them dynamically ..
        new ManifestPlugin({
            fileName: 'manifest.json',
            basePath: '/assets/'
        }),
        new CleanWebpackPlugin(['public/assets'])
    ],
    externals: {
        jquery: 'jQuery'
    },
    watch: true
};